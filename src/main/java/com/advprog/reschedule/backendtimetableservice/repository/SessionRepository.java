package com.advprog.reschedule.backendtimetableservice.repository;

import com.advprog.reschedule.backendtimetableservice.model.Session;
import com.advprog.reschedule.backendtimetableservice.model.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SessionRepository extends JpaRepository<Session, String> {
    Session findById(int id);
    Iterable<Session> findByTeacher(Teacher teacher);
    Iterable<Session> findByTeacherAndDay(Teacher teacher, String day);
}
