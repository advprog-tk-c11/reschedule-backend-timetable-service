package com.advprog.reschedule.backendtimetableservice.repository;

import com.advprog.reschedule.backendtimetableservice.model.Classes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClassesRepository extends JpaRepository<Classes, String> {
    Classes findByName(String name);
}