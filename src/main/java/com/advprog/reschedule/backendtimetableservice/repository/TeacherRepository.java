package com.advprog.reschedule.backendtimetableservice.repository;

import com.advprog.reschedule.backendtimetableservice.model.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher, String> {
    Teacher findByNrg(String nrg);
}
