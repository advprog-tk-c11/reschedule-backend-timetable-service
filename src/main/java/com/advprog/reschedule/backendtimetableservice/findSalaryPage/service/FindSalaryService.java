package com.advprog.reschedule.backendtimetableservice.findSalaryPage.service;

import java.time.LocalDate;

public interface FindSalaryService {
    long getSalary(String nrg);

    long getSimulatedSalary(String nrg, String additionType);

    void resetSimulation();

    LocalDate getSimulationDate();
}
