package com.advprog.reschedule.backendtimetableservice.findSalaryPage.core;

import com.advprog.reschedule.backendtimetableservice.model.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;

@Component
public class GetMonthlySimulatedSalary extends GetSimulatedSalary {
    @Autowired
    GetDailySimulatedSalary getDailySimulatedSalary;

    @Autowired
    GetWeeklySimulatedSalary getWeeklySimulatedSalary;

    @PostConstruct
    protected void assignObservers() {
        this.observers.add(getDailySimulatedSalary);
        this.observers.add(getWeeklySimulatedSalary);
    }

    @Override
    protected long calculateSalaryAddition(Teacher teacher) {
        long weeklyAddition = getWeeklySimulatedSalary.calculateSalaryAddition(teacher);
        long totalAddition = weeklyAddition*4;

        LocalDateTime fourWeeksAdditionDate = simulatedDate.plusDays(28);
        LocalDateTime totalAdditionDate = fourWeeksAdditionDate;
        int targetDate = getTargetDate();

        while (totalAdditionDate.getDayOfMonth() != targetDate) {
            totalAddition += getDailySimulatedSalary.calculateSalaryAddition(teacher);
            totalAdditionDate = totalAdditionDate.plusDays(1);
        }
        return totalAddition;
    }

    @Override
    protected void updateSimulatedDate() {
        LocalDateTime totalAdditionDate = simulatedDate.plusDays(28);
        int targetDate = getTargetDate();
        while (totalAdditionDate.getDayOfMonth() != targetDate) {
            totalAdditionDate = totalAdditionDate.plusDays(1);
        }
        simulatedDate = totalAdditionDate;
    }

    protected int getTargetDate() {
        int currentSimulationDate = simulatedDate.getDayOfMonth();
        int currentSimulationMonth = simulatedDate.getMonthValue();
        boolean isLeapYear = simulatedDate.getYear() % 4 == 0;
        int targetDate = currentSimulationDate;

        if (currentSimulationDate == 31) {
            if (currentSimulationMonth == 7 || currentSimulationMonth == 12) {
                targetDate = 31;
            } else {
                targetDate = 30;
            }
            if (currentSimulationMonth == 1) {
                if (isLeapYear) {
                    targetDate = 29;
                } else {
                    targetDate = 28;
                }
            }
        }
        return targetDate;
    }
}
