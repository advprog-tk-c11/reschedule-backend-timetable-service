package com.advprog.reschedule.backendtimetableservice.findSalaryPage.controller;

import com.advprog.reschedule.backendtimetableservice.findSalaryPage.core.Simulation;
import com.advprog.reschedule.backendtimetableservice.findSalaryPage.service.FindSalaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping("findSalary")
public class FindSalaryController {
    @Autowired
    private FindSalaryService findSalaryService;

    @GetMapping("")
    public Simulation findSalary(@RequestParam(value = "nrg") String nrg) {
        long salary = findSalaryService.getSalary(nrg);
        LocalDate simulationDate = LocalDate.now();
        findSalaryService.resetSimulation();

        Simulation simulation = new Simulation();
        simulation.setTeacherNrg(nrg);
        simulation.setSimulatedDate(simulationDate);
        simulation.setSimulatedSalary(salary);

        return simulation;
    }

    @GetMapping("/add-daily")
    @CrossOrigin()
    public Simulation addSalaryDaily(@RequestParam(value = "nrg") String nrg) {
        long salary = findSalaryService.getSimulatedSalary(nrg, "daily");
        LocalDate simulationDate = findSalaryService.getSimulationDate();

        Simulation simulation = new Simulation();
        simulation.setTeacherNrg(nrg);
        simulation.setSimulatedDate(simulationDate);
        simulation.setSimulatedSalary(salary);

        return simulation;
    }

    @GetMapping("/add-weekly")
    @CrossOrigin()
    public Simulation addSalaryWeekly(@RequestParam(value = "nrg") String nrg) {
        long salary = findSalaryService.getSimulatedSalary(nrg, "weekly");
        LocalDate simulationDate = findSalaryService.getSimulationDate();

        Simulation simulation = new Simulation();
        simulation.setTeacherNrg(nrg);
        simulation.setSimulatedDate(simulationDate);
        simulation.setSimulatedSalary(salary);

        return simulation;
    }

    @GetMapping("/add-monthly")
    @CrossOrigin()
    public Simulation addSalaryMonthly(@RequestParam(value = "nrg") String nrg) {
        long salary = findSalaryService.getSimulatedSalary(nrg, "monthly");
        LocalDate simulationDate = findSalaryService.getSimulationDate();

        Simulation simulation = new Simulation();
        simulation.setTeacherNrg(nrg);
        simulation.setSimulatedDate(simulationDate);
        simulation.setSimulatedSalary(salary);

        return simulation;
    }
}
