package com.advprog.reschedule.backendtimetableservice.findSalaryPage.core;

import com.advprog.reschedule.backendtimetableservice.model.Session;
import com.advprog.reschedule.backendtimetableservice.model.Teacher;
import com.advprog.reschedule.backendtimetableservice.repository.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

@Component
public class GetWeeklySimulatedSalary extends GetSimulatedSalary {
    @Autowired
    SessionRepository sessionRepository;

    @Autowired
    GetDailySimulatedSalary getDailySimulatedSalary;

    @Autowired
    GetMonthlySimulatedSalary getMonthlySimulatedSalary;

    @PostConstruct
    protected void assignObservers() {
        this.observers.add(getDailySimulatedSalary);
        this.observers.add(getMonthlySimulatedSalary);
    }

    @Override
    protected long calculateSalaryAddition(Teacher teacher) {
        Iterable<Session> sessions = sessionRepository.findByTeacher(teacher);
        int totalSessions = 0;
        for (Session s : sessions) {
            long duration = LocalTime.from(s.getStartTime()).until(s.getEndTime(), ChronoUnit.HOURS);
            totalSessions += duration;
        }
        return SALARY_PER_SESSION*totalSessions;
    }

    @Override
    protected void updateSimulatedDate() {
        simulatedDate = simulatedDate.plusDays(7);
    }
}
