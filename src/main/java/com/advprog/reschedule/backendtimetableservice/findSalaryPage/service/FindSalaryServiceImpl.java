package com.advprog.reschedule.backendtimetableservice.findSalaryPage.service;

import com.advprog.reschedule.backendtimetableservice.findSalaryPage.core.GetDailySimulatedSalary;
import com.advprog.reschedule.backendtimetableservice.findSalaryPage.core.GetMonthlySimulatedSalary;
import com.advprog.reschedule.backendtimetableservice.findSalaryPage.core.GetSimulatedSalary;
import com.advprog.reschedule.backendtimetableservice.findSalaryPage.core.GetWeeklySimulatedSalary;
import com.advprog.reschedule.backendtimetableservice.model.Teacher;
import com.advprog.reschedule.backendtimetableservice.tablePage.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class FindSalaryServiceImpl implements FindSalaryService {
    @Autowired
    private TeacherService teacherService;

    @Autowired
    private GetDailySimulatedSalary getDailySimulatedSalary;

    @Autowired
    private GetWeeklySimulatedSalary getWeeklySimulatedSalary;

    @Autowired
    private GetMonthlySimulatedSalary getMonthlySimulatedSalary;

    List<GetSimulatedSalary> observers = new ArrayList<>();

    @PostConstruct
    public void assignObservers() {
        observers.add(getDailySimulatedSalary);
        observers.add(getWeeklySimulatedSalary);
        observers.add(getMonthlySimulatedSalary);
    }

    @Override
    public long getSalary(String nrg) {
        Teacher teacher = teacherService.getTeacher(nrg);
        long salary = teacher.getTotalSalary();

        return salary;
    }

    @Override
    public long getSimulatedSalary(String nrg, String additionType) {
        GetSimulatedSalary getTotalSalary = null;

        switch (additionType) {
            case "daily":
                getTotalSalary = getDailySimulatedSalary;
                break;
            case "weekly":
                getTotalSalary = getWeeklySimulatedSalary;
                break;
            case "monthly":
                getTotalSalary = getMonthlySimulatedSalary;
                break;
        }

        Teacher teacher = teacherService.getTeacher(nrg);
        long totalSalary = getTotalSalary.getSalary(teacher);
        
        return totalSalary;
    }

    @Override
    public void resetSimulation() {
        for (GetSimulatedSalary o: observers) {
            o.setSimulatedTeacher(null);
        }
    }

    @Override
    public LocalDate getSimulationDate() {
        return getDailySimulatedSalary.getSimulatedDate().toLocalDate();
    }
}
