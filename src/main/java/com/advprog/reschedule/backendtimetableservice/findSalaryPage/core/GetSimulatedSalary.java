package com.advprog.reschedule.backendtimetableservice.findSalaryPage.core;

import com.advprog.reschedule.backendtimetableservice.model.Teacher;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public abstract class GetSimulatedSalary {
    protected long simulatedSalary;
    final protected long SALARY_PER_SESSION = 100000;
    final protected LocalDateTime TODAY = LocalDateTime.now();
    protected LocalDateTime simulatedDate = TODAY;
    protected Teacher simulatedTeacher;
    List<GetSimulatedSalary> observers = new ArrayList<>();

    public long getSalary(Teacher teacher) {
        boolean simulatedTeacherChange = (simulatedTeacher == null || !(simulatedTeacher.getNrg().equals(teacher.getNrg())));
        if (simulatedTeacherChange) {
            setSimulatedDate(TODAY);
            setSimulatedTeacher(teacher);
            setSimulatedSalary(teacher.getTotalSalary());
            notifyTeacherChange();
        }
        long addition = calculateSalaryAddition(teacher);
        setSimulatedSalary(simulatedSalary + addition);
        updateSimulatedDate();
        notifySimulationResourcesChange();
        return simulatedSalary;
    }

    protected abstract long calculateSalaryAddition(Teacher teacher);

    protected abstract void updateSimulatedDate();

    protected void notifyTeacherChange() {
        for (GetSimulatedSalary o : observers) {
            o.setSimulatedTeacher(simulatedTeacher);
            o.setSimulatedSalary(simulatedSalary);
        }
    }

    protected void notifySimulationResourcesChange() {
        for (GetSimulatedSalary o : observers) {
            o.setSimulatedDate(simulatedDate);
            o.setSimulatedSalary(simulatedSalary);
        }
    }

    public LocalDateTime getSimulatedDate() {
        return this.simulatedDate;
    }

    public Teacher getSimulatedTeacher() { return this.simulatedTeacher; }

    public void setSimulatedSalary(long newSalary) {
        this.simulatedSalary = newSalary;
    }

    public void setSimulatedTeacher(Teacher teacher) {
        this.simulatedTeacher = teacher;
    }

    public void setSimulatedDate(LocalDateTime simulatedDay) {
        this.simulatedDate = simulatedDay;
    }
}
