package com.advprog.reschedule.backendtimetableservice.findSalaryPage.core;

import com.advprog.reschedule.backendtimetableservice.model.Session;
import com.advprog.reschedule.backendtimetableservice.model.Teacher;
import com.advprog.reschedule.backendtimetableservice.repository.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalTime;
import java.time.format.TextStyle;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

@Component
public class GetDailySimulatedSalary extends GetSimulatedSalary {
    @Autowired
    SessionRepository sessionRepository;

    @Autowired
    GetWeeklySimulatedSalary getWeeklySimulatedSalary;

    @Autowired
    GetMonthlySimulatedSalary getMonthlySimulatedSalary;

    @PostConstruct
    protected void assignObservers() {
        this.observers.add(getWeeklySimulatedSalary);
        this.observers.add(getMonthlySimulatedSalary);
    }

    @Override
    protected long calculateSalaryAddition(Teacher teacher) {
        Iterable<Session> sessions = sessionRepository.findByTeacherAndDay(teacher, simulatedDate.getDayOfWeek().getDisplayName(TextStyle.FULL, new Locale("id", "ID")));
        int totalSessions = 0;
        for (Session s : sessions) {
            long duration = LocalTime.from(s.getStartTime()).until(s.getEndTime(), ChronoUnit.HOURS);
            totalSessions += duration;
        }
        return SALARY_PER_SESSION*totalSessions;
    }

    @Override
    protected void updateSimulatedDate() {
        simulatedDate = simulatedDate.plusDays(1);
    }
}
