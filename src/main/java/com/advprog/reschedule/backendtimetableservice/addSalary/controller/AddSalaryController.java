package com.advprog.reschedule.backendtimetableservice.addSalary.controller;

import com.advprog.reschedule.backendtimetableservice.tablePage.service.*;
import com.advprog.reschedule.backendtimetableservice.addSalary.service.*;
import com.advprog.reschedule.backendtimetableservice.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(path = "/simulation")
public class AddSalaryController {

    @Autowired
    private ClassesService classesService;

    @Autowired
    private AddSalaryService addSalaryService;

    @PostMapping("/next-day")
    public Iterable<Classes> simulationNextDay(@RequestBody Map<String, Object> classesData) {
        System.out.println("next-day-backend");
        String className = (String) classesData.get("className");
        String currentDay = (String) classesData.get("currentDay");
        addSalaryService.changeDay(className, currentDay);
        return classesService.getListClasses();
    }
}