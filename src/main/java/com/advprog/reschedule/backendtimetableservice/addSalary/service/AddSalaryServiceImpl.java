package com.advprog.reschedule.backendtimetableservice.addSalary.service;

import com.advprog.reschedule.backendtimetableservice.model.*;
import com.advprog.reschedule.backendtimetableservice.tablePage.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

@Service
public class AddSalaryServiceImpl implements AddSalaryService {
    @Autowired
    ClassesService classesService;

    @Autowired
    TeacherService teacherService;

    @Autowired
    private SessionService sessionService;

    @Override
    public void changeDay(String className, String day) {
        broadcast(className, day);

        Classes currentClass = classesService.getClass(className);
        if (currentClass.getCurrentDay().equals("Senin")) currentClass.setCurrentDay("Selasa");
        else if (currentClass.getCurrentDay().equals("Selasa")) currentClass.setCurrentDay("Rabu");
        else if (currentClass.getCurrentDay().equals("Rabu")) currentClass.setCurrentDay("Kamis");
        else if (currentClass.getCurrentDay().equals("Kamis")) currentClass.setCurrentDay("Jumat");
        else if (currentClass.getCurrentDay().equals("Jumat")) currentClass.setCurrentDay("Senin");

        classesService.updateClass(className, currentClass);
    }

    @Override
    public void broadcast(String className, String currentDay) {
        Classes currentClass = classesService.getClass(className);
        for(Session session : currentClass.getAvailableSessions()) {
            if(session.getDay().equals(currentDay)) {
                int idSession = session.getId();
                String idTeacher = session.getTeacher().getNrg();
                update(idSession, idTeacher);
            }
        }
    }

    @Override
    public Teacher update(int idSession, String idTeacher) {
        System.out.println("update used");
        Teacher currentTeacher = teacherService.getTeacher(idTeacher);
        Session currentSession = sessionService.getSessionById(idSession);
        LocalTime sessionStartTime = currentSession.getStartTime();
        LocalTime sessionEndTime = currentSession.getEndTime();
        long difference = (int) (ChronoUnit.HOURS.between(sessionStartTime, sessionEndTime));

        currentTeacher.setTotalSalary(currentTeacher.getTotalSalary() + (difference * 50));
        teacherService.updateTeacher(idTeacher, currentTeacher);

        return currentTeacher;
    }
}
