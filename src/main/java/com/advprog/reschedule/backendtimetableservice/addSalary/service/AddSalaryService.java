package com.advprog.reschedule.backendtimetableservice.addSalary.service;

import com.advprog.reschedule.backendtimetableservice.model.*;

public interface AddSalaryService {
    void changeDay(String className, String day);
    void broadcast(String className, String day);
    Teacher update(int idSession, String idTeacher);
}
