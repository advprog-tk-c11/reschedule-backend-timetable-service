package com.advprog.reschedule.backendtimetableservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendTimetableServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendTimetableServiceApplication.class, args);
	}

}
