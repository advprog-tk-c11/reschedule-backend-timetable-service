package com.advprog.reschedule.backendtimetableservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="classes")
@Setter
@Getter
@NoArgsConstructor
public class Classes {
    @Id
    @Column(updatable = false, nullable = false)
    private String name;

    @Column(nullable = false)
    private String currentDay;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "onClass", cascade = CascadeType.REMOVE)
    private List<Session> availableSessions;

    public Classes(String name) {
        this.name = name;
    }
}
