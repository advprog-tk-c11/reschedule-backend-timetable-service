package com.advprog.reschedule.backendtimetableservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="teacher")
@Setter
@Getter
@NoArgsConstructor
public class Teacher{
    @Id
    @Column(updatable = false, nullable = false)
    private String nrg;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private long totalSalary;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "teacher", cascade = CascadeType.REMOVE)
    private List<Session> workOnSessions;

    public Teacher(String nrg, String name) {
        this.nrg = nrg;
        this.name = name;
    }
}
