package com.advprog.reschedule.backendtimetableservice.tablePage.controller;

import com.advprog.reschedule.backendtimetableservice.model.Classes;
import com.advprog.reschedule.backendtimetableservice.model.Session;
import com.advprog.reschedule.backendtimetableservice.repository.ClassesRepository;
import com.advprog.reschedule.backendtimetableservice.tablePage.core.SessionsHour;
import com.advprog.reschedule.backendtimetableservice.tablePage.service.ClassesService;
import com.advprog.reschedule.backendtimetableservice.addSalary.service.AddSalaryService;
import com.advprog.reschedule.backendtimetableservice.tablePage.service.SessionService;
import org.apache.coyote.Response;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("timetable")
public class ClassesController {
    @Autowired
    private ClassesService classesService;

    @Autowired
    private SessionService sessionService;

    @Autowired
    private AddSalaryService addSalaryService;

    @Autowired
    private ClassesRepository classesRepository;

    @CrossOrigin()
    @PostMapping("/newClass")
    public Classes createClass(@RequestBody Map<String, Object> classesData){
        if(classesRepository.findByName(classesData.get("name").toString()) != null)
            throw new DataIntegrityViolationException("Kelas dengan nama tersebut sudah ada.");
        Classes newClasses = new Classes();
        newClasses.setName(classesData.get("name").toString());
        newClasses.setCurrentDay("Senin");

        return classesService.createClass(newClasses);
    }

    @PostMapping("/updateClass")
    @CrossOrigin()
    public void updateClass(@RequestBody Map<String, Object> classesData){
        Classes currentClass = classesRepository.findByName(classesData.get("name").toString());
        String className = currentClass.getName();
        String currentDay = currentClass.getCurrentDay();
        
        addSalaryService.changeDay(className, currentDay);
    }

    @CrossOrigin()
    @PutMapping("/updateClass/{name}")
    public Classes updateClassFullData(@PathVariable(value = "name") String classToBeUpdatedName, @RequestBody Map<String, Object> newClassData) throws Exception {
        try {
            Classes classToBeUpdate = classesRepository.findByName(classToBeUpdatedName);
            Classes newClass = new Classes();
            if(newClassData.get("currentDay") != null) {
                String newClassCurrentDay = newClassData.get("currentDay").toString();
                newClass.setCurrentDay(newClassCurrentDay);
            }
            else {
                newClass.setCurrentDay(classToBeUpdate.getCurrentDay());
            }
            List<Session> classToBeUpdateAvailableSession = classToBeUpdate.getAvailableSessions();
            classesService.deleteClass(classToBeUpdatedName);
            String newClassName = newClassData.get("name").toString();
            newClass = classesService.updateClass(newClassName, newClass);
            for(Session session: classToBeUpdateAvailableSession) {
                String nrg = session.getTeacher().getNrg();
                try {
                    sessionService.createSession(session, nrg, newClassName);
                }
                catch(Exception e) {
                    throw e;
                }
            }
            return newClass;
        } catch(RuntimeException e) {
            throw e;
        }
    }

    @CrossOrigin()
    @GetMapping("/allClass")
    public Iterable<Classes> getListClasses() {
        return classesService.getListClasses();
    }

    @CrossOrigin()
    @DeleteMapping("/deleteClass/{name}")
    public ResponseEntity deleteClass(@PathVariable(value = "name") String classToBeDeleteName) {
        classesService.deleteClass(classToBeDeleteName);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @CrossOrigin()
    @GetMapping("/allClassSessions")
    public String getClassSessions(@RequestParam("className") String className) throws JsonProcessingException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        List<SessionsHour> resultSessionsHourList = classesService.getSessionsByName(className);
        String json = ow.writeValueAsString(resultSessionsHourList);
        resultSessionsHourList.clear();
        System.gc();
        return json;
    }
}