package com.advprog.reschedule.backendtimetableservice.tablePage.service;

import com.advprog.reschedule.backendtimetableservice.model.Classes;
import com.advprog.reschedule.backendtimetableservice.model.Session;
import com.advprog.reschedule.backendtimetableservice.tablePage.core.SessionsHour;

import java.util.List;

public interface ClassesService {
    Classes createClass(Classes newClass);

    Classes getClass(String className);

    Iterable<Classes> getListClasses();

    Classes updateClass(String className, Classes classes);

    void deleteClass(String className);

    List<SessionsHour> getSessionsByName(String className);

    List<SessionsHour> generateSessionsHour();

    List<SessionsHour> setSessionsIntoSessionsHour(List<Session> sessions, List<SessionsHour> sessionsHours);

    int indexValueOfStartTime(String startTime);

    void setDaySession(SessionsHour sessionsHour, Session session);
}