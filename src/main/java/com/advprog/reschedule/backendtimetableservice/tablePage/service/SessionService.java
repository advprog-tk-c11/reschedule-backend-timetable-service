package com.advprog.reschedule.backendtimetableservice.tablePage.service;

import com.advprog.reschedule.backendtimetableservice.model.Session;

public interface SessionService {
    Session createSession(Session session, String nrg, String name) throws Exception;

    Session getSessionById(int id);

    Iterable<Session> getListSession();

    Session updateSession(int id, Session session) throws Exception;

    void deleteSessionById(int id);
}

