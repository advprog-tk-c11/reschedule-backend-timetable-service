package com.advprog.reschedule.backendtimetableservice.tablePage.controller;

import com.advprog.reschedule.backendtimetableservice.model.Classes;
import com.advprog.reschedule.backendtimetableservice.model.Session;
import com.advprog.reschedule.backendtimetableservice.model.Teacher;
import com.advprog.reschedule.backendtimetableservice.repository.ClassesRepository;
import com.advprog.reschedule.backendtimetableservice.repository.SessionRepository;
import com.advprog.reschedule.backendtimetableservice.repository.TeacherRepository;
import com.advprog.reschedule.backendtimetableservice.tablePage.service.SessionService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalTime;
import java.util.Map;

@RestController
@RequestMapping("timetable")
public class SessionController {
    @Autowired
    private SessionService sessionService;

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private ClassesRepository classesRepository;

    @PostMapping("/newSession")
    @CrossOrigin()
    public Session createSession(@RequestBody Map<String, Object> sessionData) throws Exception {
        try {
            Session newSession = new Session();
            newSession.setCourse(sessionData.get("course").toString());
            newSession.setDay(sessionData.get("day").toString());
            newSession.setStartTime(LocalTime.parse(sessionData.get("startTime").toString()));
            newSession.setEndTime(LocalTime.parse(sessionData.get("endTime").toString()));
            String sessionTeacher = sessionData.get("nrg").toString();
            String sessionClasses = sessionData.get("name").toString();

            return sessionService.createSession(newSession, sessionTeacher, sessionClasses);
        } catch(Exception e) {
            throw e;
        }
    }

    @CrossOrigin()
    @GetMapping("/allSession")
    public Iterable<Session> getListSession() {
        return sessionService.getListSession();
    }

    @CrossOrigin()
    @PutMapping("/updateSession/{id}")
    public Session updateSession(@PathVariable(value = "id") int sessionToBeUpdateId, @RequestBody Map<String, Object> newSessionData) throws Exception {
        try {
            String newSessionCourse = newSessionData.get("course").toString();
            String newSessionDay = newSessionData.get("day").toString();
            String newSessionStartTime = newSessionData.get("startTime").toString();
            String newSessionEndTime = newSessionData.get("endTime").toString();

            Session newSession = new Session();
            newSession.setCourse(newSessionCourse);
            newSession.setDay(newSessionDay);
            newSession.setStartTime(LocalTime.parse(newSessionStartTime));
            newSession.setEndTime(LocalTime.parse(newSessionEndTime));
            String newSessionTeacherNrg = newSessionData.get("nrg").toString();
            Teacher newSessionTeacher = teacherRepository.findByNrg(newSessionTeacherNrg);
            newSession.setTeacher(newSessionTeacher);
            String newSessionClassName = newSessionData.get("name").toString();
            Classes newSessionClasses = classesRepository.findByName(newSessionClassName);
            newSession.setOnClass(newSessionClasses);
            return sessionService.updateSession(sessionToBeUpdateId, newSession);
        } catch(RuntimeException e) {
            throw e;
        }
    }

    @CrossOrigin()
    @DeleteMapping("/deleteSession/{id}")
    public ResponseEntity deleteSession(@PathVariable(value = "id") int sessionToBeDeleteId) {
        sessionService.deleteSessionById(sessionToBeDeleteId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}