package com.advprog.reschedule.backendtimetableservice.tablePage.controller;

import com.advprog.reschedule.backendtimetableservice.model.Session;
import com.advprog.reschedule.backendtimetableservice.model.Teacher;
import com.advprog.reschedule.backendtimetableservice.repository.TeacherRepository;
import com.advprog.reschedule.backendtimetableservice.tablePage.service.SessionService;
import com.advprog.reschedule.backendtimetableservice.tablePage.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("timetable")
public class TeacherController {
    @Autowired
    private TeacherService teacherService;

    @Autowired
    private SessionService sessionService;

    @Autowired
    private TeacherRepository teacherRepository;

    @CrossOrigin()
    @PostMapping("/newTeacher")
    public Teacher createTeacher(@RequestBody Map<String, Object> teacherData){
        if(teacherRepository.findByNrg(teacherData.get("nrg").toString()) != null)
            throw new DataIntegrityViolationException("Guru dengan NRG tersebut sudah ada");
        Teacher newTeacher = new Teacher();
        newTeacher.setNrg(teacherData.get("nrg").toString());
        newTeacher.setName(teacherData.get("name").toString());
        newTeacher.setTotalSalary(0);
        return teacherService.createTeacher(newTeacher);
    }

    @CrossOrigin()
    @GetMapping("/allTeacher")
    public Iterable<Teacher> getListTeacher() {
        return teacherService.getListTeacher();
    }

    @CrossOrigin()
    @PutMapping("/updateTeacher/{nrg}")
    public Teacher updateTeacher(@PathVariable(value = "nrg") String teacherToBeUpdateNrg, @RequestBody Map<String, Object> newTeacherData) throws Exception {
        try {
            Teacher teacherToBeUpdate = teacherRepository.findByNrg(teacherToBeUpdateNrg);
            String newTeacherName = newTeacherData.get("name").toString();
            Long newTeacherTotalSalary = Long.valueOf(newTeacherData.get("totalSalary").toString());
            Teacher newTeacher = new Teacher();
            newTeacher.setName(newTeacherName);
            newTeacher.setTotalSalary(newTeacherTotalSalary);
            List<Session> teacherToBeUpdateWorkOnSessions = teacherToBeUpdate.getWorkOnSessions();
            teacherService.deleteTeacher(teacherToBeUpdateNrg);
            String newTeacherNrg = newTeacherData.get("nrg").toString();
            newTeacher = teacherService.updateTeacher(newTeacherNrg, newTeacher);
            for(Session session: teacherToBeUpdateWorkOnSessions) {
                String className = session.getOnClass().getName();
                try {
                    sessionService.createSession(session, newTeacherNrg, className);
                }
                catch(Exception e) {
                    throw e;
                }
            }
            return newTeacher;
        } catch(RuntimeException e) {
            throw e;
        }
    }

    @CrossOrigin()
    @DeleteMapping("/deleteTeacher/{nrg}")
    public ResponseEntity deleteTeacher(@PathVariable(value = "nrg") String teacherToBeDeleteNrg) {
        teacherService.deleteTeacher(teacherToBeDeleteNrg);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}