package com.advprog.reschedule.backendtimetableservice.tablePage.service;

import com.advprog.reschedule.backendtimetableservice.model.Teacher;
import com.advprog.reschedule.backendtimetableservice.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeacherServiceImpl implements TeacherService {
    @Autowired
    private TeacherRepository teacherRepository;

    @Override
    public Teacher createTeacher(Teacher newTeacher) {
        teacherRepository.save(newTeacher);
        return newTeacher;
    }

    @Override
    public Teacher getTeacher(String nrg) {
        return teacherRepository.findByNrg(nrg);
    }

    @Override
    public Iterable<Teacher> getListTeacher() {
        return teacherRepository.findAll();
    }

    @Override
    public Teacher updateTeacher(String teacherToBeUpdateNrg, Teacher newTeacher) {
        newTeacher.setNrg(teacherToBeUpdateNrg);
        teacherRepository.save(newTeacher);
        return newTeacher;
    }

    @Override
    public void deleteTeacher(String nrg) {
        Teacher teacher = teacherRepository.findByNrg(nrg);
        teacherRepository.delete(teacher);
    }
}