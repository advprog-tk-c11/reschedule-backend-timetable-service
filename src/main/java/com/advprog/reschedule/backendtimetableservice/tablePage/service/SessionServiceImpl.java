package com.advprog.reschedule.backendtimetableservice.tablePage.service;

import com.advprog.reschedule.backendtimetableservice.model.Classes;
import com.advprog.reschedule.backendtimetableservice.model.Session;
import com.advprog.reschedule.backendtimetableservice.model.Teacher;
import com.advprog.reschedule.backendtimetableservice.repository.ClassesRepository;
import com.advprog.reschedule.backendtimetableservice.repository.SessionRepository;

import com.advprog.reschedule.backendtimetableservice.repository.TeacherRepository;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalTime;
import java.util.List;

@Service
public class SessionServiceImpl implements SessionService {
    @Autowired
    ClassesRepository classesRepository;

    @Autowired
    TeacherRepository teacherRepository;

    @Autowired
    SessionRepository sessionRepository;

    @Override
    public Session createSession(Session session, String nrg, String name) throws Exception {
        Classes sessionClass = classesRepository.findByName(name);
        Teacher sessionTeacher = teacherRepository.findByNrg(nrg);
        if(isTimeColission(session, sessionClass, sessionTeacher))
            throw new Exception("Sesi tersebut memiliki jadwal yang bertubrukan. Silahkan buat sesi dengan jadwal yang lain.");
        session.setOnClass(sessionClass);
        session.setTeacher(sessionTeacher);
        sessionRepository.save(session);

        return session;
    }

    @Override
    public Session getSessionById(int id) {
        return sessionRepository.findById(id);
    }

    @Override
    public Iterable<Session> getListSession() {
        return sessionRepository.findAll();
    }

    @Override
    public Session updateSession(int id, Session session) throws Exception {
        session.setId(id);
        if(isTimeColission(session, session.getOnClass(), session.getTeacher()))
            throw new Exception("Sesi tersebut memiliki jadwal yang bertubrukan. Silahkan buat sesi dengan jadwal yang lain.");
        sessionRepository.save(session);
        return session;
    }

    @Override
    public void deleteSessionById(int id) {
        Session session = getSessionById(id);
        sessionRepository.delete(session);
    }

    private Boolean isTimeColission(Session session, Classes sessionClass, Teacher sessionTeacher) {
        String sessionDay = session.getDay();
        LocalTime sessionStartTime = session.getStartTime();
        LocalTime sessionEndTime = session.getEndTime();
        List<Session> classSessions = sessionClass.getAvailableSessions();
        for(Session thisSession : classSessions) {
            if(thisSession.getDay().equals(sessionDay)) {
                LocalTime thisSessionStartTime = thisSession.getStartTime();
                LocalTime thisSessionEndTime = thisSession.getEndTime();
                if(sessionStartTime.isAfter(thisSessionStartTime) && sessionStartTime.isBefore(thisSessionEndTime) || sessionEndTime.isAfter(thisSessionStartTime) && sessionEndTime.isBefore(thisSessionEndTime) || sessionStartTime.equals(thisSessionStartTime) || sessionEndTime.equals(thisSessionEndTime) || sessionStartTime.isBefore(thisSessionStartTime) && sessionEndTime.isAfter(thisSessionEndTime)) {
                    return true;
                }
            }
        }
        List<Session> teacherSessions = sessionTeacher.getWorkOnSessions();
        for(Session thisSession : teacherSessions) {
            if(thisSession.getDay().equals(sessionDay)) {
                LocalTime thisSessionStartTime = thisSession.getStartTime();
                LocalTime thisSessionEndTime = thisSession.getEndTime();
                if(sessionStartTime.isAfter(thisSessionStartTime) && sessionStartTime.isBefore(thisSessionEndTime) || sessionEndTime.isAfter(thisSessionStartTime) && sessionEndTime.isBefore(thisSessionEndTime) || sessionStartTime.equals(thisSessionStartTime) || sessionEndTime.equals(thisSessionEndTime) || sessionStartTime.isBefore(thisSessionStartTime) && sessionEndTime.isAfter(thisSessionEndTime)) {
                    return true;
                }
            }
        }
        return false;
    }
}
