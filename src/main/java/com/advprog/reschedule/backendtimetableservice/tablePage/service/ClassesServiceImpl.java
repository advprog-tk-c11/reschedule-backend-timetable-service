package com.advprog.reschedule.backendtimetableservice.tablePage.service;

import com.advprog.reschedule.backendtimetableservice.model.Classes;
import com.advprog.reschedule.backendtimetableservice.model.Session;
import com.advprog.reschedule.backendtimetableservice.repository.ClassesRepository;
import com.advprog.reschedule.backendtimetableservice.tablePage.core.SessionsHour;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.config.ConfigDataResourceNotFoundException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class ClassesServiceImpl implements ClassesService {
    @Autowired
    private ClassesRepository classesRepository;

    @Override
    public Classes createClass(Classes newClass) {
        classesRepository.save(newClass);
        return newClass;
    }

    @Override
    public Classes getClass(String className) {
        return classesRepository.findByName(className);
    }

    @Override
    public Iterable<Classes> getListClasses() {
        return classesRepository.findAll();
    }

    @Override
    public Classes updateClass(String className, Classes classes) {
        classes.setName(className);
        classesRepository.save(classes);
        return classes;
    }

    @Override
    public void deleteClass(String className) {
        Classes targetClass = classesRepository.findByName(className);
        classesRepository.delete(targetClass);
    }

    @Override
    public List<SessionsHour> getSessionsByName(String className) {
        Classes thisClass = classesRepository.findByName(className);
        List<Session> thisClassSessions = thisClass.getAvailableSessions();
        List<SessionsHour> thisClassSessionsHour = generateSessionsHour();
        return setSessionsIntoSessionsHour(thisClassSessions, thisClassSessionsHour);
    }

    @Override
    public List<SessionsHour> generateSessionsHour() {
        List<SessionsHour> sessionsHourList = new ArrayList<SessionsHour>();
        for(int startHour = 8; startHour <= 14; startHour++) {
            if(startHour == 12) continue;
            String startTime = startHour + ":00";
            String endTime = startHour + 1 + ":00";
            sessionsHourList.add(new SessionsHour(startTime, endTime));
        }

        return sessionsHourList;
    }

    @Override
    public List<SessionsHour> setSessionsIntoSessionsHour(List<Session> sessions, List<SessionsHour> sessionsHours) {
        for(Session session: sessions) {
            LocalTime startTime = session.getStartTime();
            int indexOfSessionsHour = indexValueOfStartTime(startTime.toString());
            SessionsHour currentSessionsHour = sessionsHours.get(indexOfSessionsHour);
            setDaySession(currentSessionsHour, session);
        }
        return sessionsHours;
    }

    @Override
    public int indexValueOfStartTime(String startTime) {
        int indexValue = 0;
        switch(startTime) {
            case "08:00":
                indexValue = 0;
                break;
            case "09:00":
                indexValue = 1;
                break;
            case "10:00":
                indexValue = 2;
                break;
            case "11:00":
                indexValue = 3;
                break;
            case "13:00":
                indexValue = 4;
                break;
            case "14:00":
                indexValue = 5;
                break;
        }
        return indexValue;
    }

    @Override
    public void setDaySession(SessionsHour sessionsHour, Session session) {
        switch(session.getDay()) {
            case "Senin":
                sessionsHour.setMondaySession(session);
                break;
            case "Selasa":
                sessionsHour.setTuesdaySession(session);
                break;
            case "Rabu":
                sessionsHour.setWednesdaySession(session);
                break;
            case "Kamis":
                sessionsHour.setThursdaySession(session);
                break;
            case "Jumat":
                sessionsHour.setFridaySession(session);
                break;
        }
    }
}