package com.advprog.reschedule.backendtimetableservice.tablePage.service;

import com.advprog.reschedule.backendtimetableservice.model.Teacher;

public interface TeacherService {
    Teacher createTeacher(Teacher newTeacher);

    Teacher getTeacher(String teacher);

    Iterable<Teacher> getListTeacher();

    Teacher updateTeacher(String teacherToBeUpdateNrg, Teacher newTeacher);

    void deleteTeacher(String nrg);
}

