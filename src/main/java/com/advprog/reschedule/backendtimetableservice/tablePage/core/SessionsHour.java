package com.advprog.reschedule.backendtimetableservice.tablePage.core;

import com.advprog.reschedule.backendtimetableservice.model.Session;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class SessionsHour {

    private String startTime;

    private String endTime;

    private Session mondaySession;

    private Session tuesdaySession;

    private Session wednesdaySession;

    private Session thursdaySession;

    private Session fridaySession;

    public SessionsHour(String startTime, String endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }
}
