package com.advprog.reschedule.backendtimetableservice.addSalary.controller;

import com.advprog.reschedule.backendtimetableservice.addSalary.service.AddSalaryService;
import com.advprog.reschedule.backendtimetableservice.model.Classes;
import com.advprog.reschedule.backendtimetableservice.model.Session;
import com.advprog.reschedule.backendtimetableservice.model.Teacher;
import com.advprog.reschedule.backendtimetableservice.repository.ClassesRepository;
import com.advprog.reschedule.backendtimetableservice.tablePage.controller.ClassesController;
import com.advprog.reschedule.backendtimetableservice.tablePage.service.ClassesServiceImpl;
import com.advprog.reschedule.backendtimetableservice.tablePage.service.SessionServiceImpl;
import com.advprog.reschedule.backendtimetableservice.tablePage.service.TeacherServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalTime;
import java.util.Arrays;

import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = AddSalaryController.class)
public class AddSalaryControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ClassesRepository classesRepository;

    @MockBean
    private ClassesServiceImpl classesService;

    @MockBean
    private SessionServiceImpl sessionService;

    @MockBean
    private AddSalaryService addSalaryService;

    private Classes classForTest;

    private Session sessionForTest;

    private Teacher teacherForTest;

    @BeforeEach
    public void setUp(){
        classForTest = new Classes();
        classForTest.setName("Dummy Class");
        classForTest.setCurrentDay("Senin");

        teacherForTest = new Teacher();
        teacherForTest.setNrg("1");
        teacherForTest.setName("Dummy Teacher");

        sessionForTest = new Session();
        sessionForTest.setCourse("Dummy Course");
        sessionForTest.setDay("Senin");
        sessionForTest.setStartTime(LocalTime.parse("08:00:00"));
        sessionForTest.setEndTime(LocalTime.parse("09:00:00"));
        sessionForTest.setOnClass(classForTest);
        sessionForTest.setTeacher(teacherForTest);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }


//    @Test
//    public void testControllerAddSalaryAfterDayChanged() throws Exception {
//        when(addSalaryService.changeDay(classForTest.getName(), classForTest.getCurrentDay())).thenReturn(classForTest);
//
//    }
}
