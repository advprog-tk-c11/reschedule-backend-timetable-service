package com.advprog.reschedule.backendtimetableservice.addSalary.service;

import com.advprog.reschedule.backendtimetableservice.findSalaryPage.core.GetDailySimulatedSalary;
import com.advprog.reschedule.backendtimetableservice.findSalaryPage.core.GetMonthlySimulatedSalary;
import com.advprog.reschedule.backendtimetableservice.findSalaryPage.core.GetWeeklySimulatedSalary;
import com.advprog.reschedule.backendtimetableservice.findSalaryPage.service.FindSalaryServiceImpl;
import com.advprog.reschedule.backendtimetableservice.model.Classes;
import com.advprog.reschedule.backendtimetableservice.model.Session;
import com.advprog.reschedule.backendtimetableservice.model.Teacher;
import com.advprog.reschedule.backendtimetableservice.repository.ClassesRepository;
import com.advprog.reschedule.backendtimetableservice.repository.SessionRepository;
import com.advprog.reschedule.backendtimetableservice.repository.TeacherRepository;
import com.advprog.reschedule.backendtimetableservice.tablePage.service.TeacherServiceImpl;
import com.advprog.reschedule.backendtimetableservice.tablePage.service.SessionServiceImpl;
import com.advprog.reschedule.backendtimetableservice.addSalary.service.AddSalaryServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class AddSalaryServiceTest {

    @Mock
    private TeacherRepository teacherRepository;

    @Mock
    private SessionRepository sessionRepository;

    @Mock
    private TeacherServiceImpl teacherService;

    @Mock
    private SessionServiceImpl sessionService;

    @Mock
    private AddSalaryServiceImpl addSalaryService;

    @InjectMocks
    private FindSalaryServiceImpl findSalaryService;

    private Classes classForTest;

    private Session sessionForTest;

    private Teacher teacherForTest;

    @BeforeEach
    public void setUp(){
        classForTest = new Classes();
        classForTest.setName("Dummy Class");
        classForTest.setCurrentDay("Senin");

        teacherForTest = new Teacher();
        teacherForTest.setNrg("1");
        teacherForTest.setName("Dummy Teacher");

        sessionForTest = new Session();
        sessionForTest.setCourse("Dummy Course");
        sessionForTest.setDay("Senin");
        sessionForTest.setStartTime(LocalTime.parse("08:00:00"));
        sessionForTest.setEndTime(LocalTime.parse("09:00:00"));
        sessionForTest.setOnClass(classForTest);
        sessionForTest.setTeacher(teacherForTest);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

//    @Test
//    public void testUpdateMethodWorks() throws Exception {
//        lenient().when(teacherService.getTeacher(teacherForTest.getNrg())).thenReturn(teacherForTest);
//        lenient().when(sessionService.getSessionById(sessionForTest.getId())).thenReturn(sessionForTest);
//        lenient().when((sessionService.getSessionById(sessionForTest.getId())).getStartTime()).thenReturn(sessionForTest.getStartTime());
//        lenient().when((sessionService.getSessionById(sessionForTest.getId())).getEndTime()).thenReturn(sessionForTest.getEndTime());
//
//        Teacher teacher = addSalaryService.update(sessionForTest.getId(), teacherForTest.getNrg());
//
//        assertEquals(50, teacher.getTotalSalary());
//    }
//
//    @Test
//    public void testBroadcastMethodWorks() throws Exception {
//
//    }
//
//    @Test
//    public void testChangeDayMethodWorks() throws Exception {
//
//    }
}
