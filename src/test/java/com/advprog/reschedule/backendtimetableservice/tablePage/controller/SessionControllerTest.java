package com.advprog.reschedule.backendtimetableservice.tablePage.controller;

import com.advprog.reschedule.backendtimetableservice.model.Classes;
import com.advprog.reschedule.backendtimetableservice.model.Session;
import com.advprog.reschedule.backendtimetableservice.model.Teacher;
import com.advprog.reschedule.backendtimetableservice.repository.ClassesRepository;
import com.advprog.reschedule.backendtimetableservice.repository.SessionRepository;
import com.advprog.reschedule.backendtimetableservice.repository.TeacherRepository;
import com.advprog.reschedule.backendtimetableservice.tablePage.service.SessionServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalTime;
import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = SessionController.class)
public class SessionControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private SessionRepository sessionRepository;

    @MockBean
    private TeacherRepository teacherRepository;

    @MockBean
    private ClassesRepository classesRepository;

    @MockBean
    private SessionServiceImpl sessionService;

    private Session sessionForTest;

    private Classes classForTest;

    private Teacher teacherForTest;

    @BeforeEach
    public void setUp() {
        classForTest = new Classes();
        classForTest.setName("Dummy Class");
        classForTest.setCurrentDay("Senin");
        teacherForTest = new Teacher();
        teacherForTest.setNrg("1");
        teacherForTest.setName("Dummy Teacher");
        teacherForTest.setTotalSalary(0);
        sessionForTest = new Session();
        sessionForTest.setId(0);
        sessionForTest.setCourse("Dummy Course");
        sessionForTest.setDay("Senin");
        sessionForTest.setStartTime(LocalTime.parse("08:00:00"));
        sessionForTest.setEndTime(LocalTime.parse("09:00:00"));
        sessionForTest.setTeacher(teacherForTest);
        sessionForTest.setOnClass(classForTest);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testControllerCreateSession() throws Exception {
        when(sessionService.createSession(sessionForTest, teacherForTest.getNrg(), classForTest.getName())).thenReturn(sessionForTest);

        mvc.perform(post("/timetable/newSession")
                .contentType(MediaType.APPLICATION_JSON_VALUE));
    }

    @Test
    void testControllerGetListSession() throws Exception {
        Iterable<Session> listSession = Arrays.asList(sessionForTest);
        when(sessionService.getListSession()).thenReturn(listSession);

        mvc.perform(get("/timetable/allSession").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(sessionForTest.getId()));
    }

//    @Test
//    void testControllerUpdateSession() throws Exception {
//        sessionService.createSession(sessionForTest, teacherForTest.getNrg(), classForTest.getName());
//
//        String sessionUpdatedCourse = "Dummy Course U";
//        sessionForTest.setCourse(sessionUpdatedCourse);
//
//        when(sessionService.updateSession(sessionForTest.getId(), sessionForTest)).thenReturn(sessionForTest);
//
//        mvc.perform(put("/timetable/updateSession/" + sessionForTest.getId()).contentType(MediaType.APPLICATION_JSON)
//                .content(mapToJson(sessionForTest)))
//                .andExpect(status().isOk()).andExpect(content()
//                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.course").value(sessionUpdatedCourse));
//    }

    @Test
    void testControllerDeleteSession() throws Exception {
        sessionService.createSession(sessionForTest, teacherForTest.getNrg(), classForTest.getName());
        mvc.perform(delete("/timetable/deleteSession/" + sessionForTest.getId()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }
}
