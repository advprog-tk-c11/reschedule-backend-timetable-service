package com.advprog.reschedule.backendtimetableservice.tablePage.controller;

import com.advprog.reschedule.backendtimetableservice.addSalary.service.AddSalaryService;
import com.advprog.reschedule.backendtimetableservice.model.Classes;
import com.advprog.reschedule.backendtimetableservice.repository.ClassesRepository;
import com.advprog.reschedule.backendtimetableservice.tablePage.service.ClassesServiceImpl;
import com.advprog.reschedule.backendtimetableservice.tablePage.service.SessionServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = ClassesController.class)
public class ClassesControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private ClassesRepository classesRepository;

    @MockBean
    private ClassesServiceImpl classesService;

    @MockBean
    private SessionServiceImpl sessionService;

    @MockBean
    private AddSalaryService addSalaryService;

    private Classes classForTest;

    @BeforeEach
    public void setUp() {
        classForTest = new Classes("Dummy Class");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testControllerCreateClass() throws Exception {
        when(classesService.createClass(classForTest)).thenReturn(classForTest);

        mvc.perform(post("/timetable/newClass")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapToJson(classForTest)));
    }

        @Test
    void testControllerGetListClass() throws Exception {
        Iterable<Classes> listClass = Arrays.asList(classForTest);
        when(classesService.getListClasses()).thenReturn(listClass);

        mvc.perform(get("/timetable/allClass").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].name").value(classForTest.getName()));
    }


    @Test
    void testControllerDeleteClass() throws Exception {
        classesService.createClass(classForTest);
        mvc.perform(delete("/timetable/deleteClass/" + classForTest.getName()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

//    @Test
//    void testControllerUpdateClass() throws Exception {
//        classesService.createClass(classForTest);
//
//        String classUpdatedName = "Dummy Class U";
//        classForTest.setName(classUpdatedName);
//
//        when(classesService.updateClass(classForTest.getName(), classForTest)).thenReturn(classForTest);
//
//        mvc.perform(put("/timetable/updateClass/" + classForTest.getName()).contentType(MediaType.APPLICATION_JSON)
//                .content(mapToJson(classForTest)))
//                .andExpect(status().isOk()).andExpect(content()
//                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.name").value(classUpdatedName));
//    }

    @Test
    void testControllerGetClassSession() throws Exception {
        mvc.perform(get("/timetable/allClassSessions?className=" + classForTest.getName()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}