package com.advprog.reschedule.backendtimetableservice.tablePage.service;

import com.advprog.reschedule.backendtimetableservice.model.Teacher;
import com.advprog.reschedule.backendtimetableservice.repository.TeacherRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class TeacherServiceImplTest {
    @Mock
    private TeacherRepository teacherRepository;

    @InjectMocks
    private TeacherServiceImpl teacherService;

    private Teacher teacher;

    @BeforeEach
    public void setUp() {
        teacher = new Teacher();
        teacher.setNrg("1");
        teacher.setName("Dummy Teacher");
        teacher.setTotalSalary(0);
    }

    @Test
    void testServiceCreateTeacher(){
        lenient().when(teacherService.createTeacher(teacher)).thenReturn(teacher);
        Teacher resultTeacher = teacherService.createTeacher(teacher);
        Assertions.assertEquals(teacher.getNrg(), resultTeacher.getNrg());
    }

    @Test
    void testServiceGetTeacherByNrg() {
        lenient().when(teacherService.getTeacher("1")).thenReturn(teacher);
        Teacher fetchedTeacher = teacherService.getTeacher("1");
        Assertions.assertEquals(teacher.getNrg(), fetchedTeacher.getNrg());
    }

    @Test
    void testServiceGetListTeacher() {
        Iterable<Teacher> listTeacher = teacherRepository.findAll();
        lenient().when(teacherService.getListTeacher()).thenReturn(listTeacher);
        Iterable<Teacher> listTeacherResult = teacherService.getListTeacher();
        Assertions.assertIterableEquals(listTeacher, listTeacherResult);
    }

    @Test
    void testServiceUpdateTeacher() {
        teacherService.createTeacher(teacher);
        String teacherNameBeforeUpdate = teacher.getName();
        String newTeacherName = "Dummy Teacher 2";
        long newTeacherTotalSalary = 10;
        teacher.setName(newTeacherName);
        teacher.setTotalSalary(newTeacherTotalSalary);
        Teacher expectedTeacher = new Teacher();
        expectedTeacher.setName(newTeacherName);
        expectedTeacher.setTotalSalary(newTeacherTotalSalary);
        expectedTeacher.setWorkOnSessions(teacher.getWorkOnSessions());
        lenient().when(teacherService.updateTeacher(teacher.getNrg(), expectedTeacher)).thenReturn(teacher);
        Teacher resultTeacher = teacherService.updateTeacher(teacher.getNrg(), expectedTeacher);
        Assertions.assertNotEquals(resultTeacher.getName(), teacherNameBeforeUpdate);
        Assertions.assertEquals(teacher.getName(), resultTeacher.getName());
    }

    @Test
    void testServiceDeleteTeacher() {
        teacherService.createTeacher(teacher);
        teacherService.deleteTeacher(teacher.getNrg());
        lenient().when(teacherRepository.findByNrg(teacher.getNrg())).thenReturn(null);
        Assertions.assertEquals(null, teacherRepository.findByNrg(teacher.getNrg()));
    }
}