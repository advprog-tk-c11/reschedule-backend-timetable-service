package com.advprog.reschedule.backendtimetableservice.tablePage.controller;

import com.advprog.reschedule.backendtimetableservice.model.Classes;
import com.advprog.reschedule.backendtimetableservice.model.Session;
import com.advprog.reschedule.backendtimetableservice.model.Teacher;
import com.advprog.reschedule.backendtimetableservice.repository.ClassesRepository;
import com.advprog.reschedule.backendtimetableservice.repository.SessionRepository;
import com.advprog.reschedule.backendtimetableservice.repository.TeacherRepository;
import com.advprog.reschedule.backendtimetableservice.tablePage.service.ClassesServiceImpl;
import com.advprog.reschedule.backendtimetableservice.tablePage.service.SessionServiceImpl;
import com.advprog.reschedule.backendtimetableservice.tablePage.service.TeacherServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = TeacherController.class)
public class TeacherControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private TeacherRepository teacherRepository;

    @MockBean
    private ClassesRepository classesRepository;

    @MockBean
    private SessionRepository sessionRepository;

    @MockBean
    private TeacherServiceImpl teacherService;

    @MockBean
    private SessionServiceImpl sessionService;

    @MockBean
    private ClassesServiceImpl classesService;

    private Teacher teacherForTest;

    private Session sessionForTest;

    private Classes classForTest;

    @BeforeEach
    public void setUp() {
        teacherForTest = new Teacher("1", "Dummy Teacher");
        teacherForTest.setTotalSalary(0);

        classForTest = new Classes();
        classForTest.setName("Dummy Class");
        classForTest.setCurrentDay("Senin");

        sessionForTest = new Session();
        sessionForTest.setId(0);
        sessionForTest.setCourse("Dummy Course");
        sessionForTest.setDay("Senin");
        sessionForTest.setStartTime(LocalTime.parse("08:00:00"));
        sessionForTest.setEndTime(LocalTime.parse("09:00:00"));
        sessionForTest.setTeacher(teacherForTest);
        sessionForTest.setOnClass(classForTest);

        List<Session> teacherWorkOnSessionList = new ArrayList<Session>();
        teacherWorkOnSessionList.add(sessionForTest);
        teacherForTest.setWorkOnSessions(teacherWorkOnSessionList);

        teacherRepository.save(teacherForTest);
        classesRepository.save(classForTest);
        sessionRepository.save(sessionForTest);

        MockitoAnnotations.initMocks(this);

//        sessionService.createSession(sessionForTest, teacherForTest.getNrg(), classForTest.getName());
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testControllerCreateTeacher() throws Exception {
        when(teacherService.createTeacher(teacherForTest)).thenReturn(teacherForTest);

        mvc.perform(post("/timetable/newTeacher")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(teacherForTest)));
    }

//    @Test
//    void testControllerUpdateTeacher() throws Exception {
//        teacherService.createTeacher(teacherForTest);
//
//        String teacherUpdatedName = "Dummy Teacher U";
//        Long teacherUpdatedTotalSalary = Long.valueOf(15000);
//        teacherForTest.setName(teacherUpdatedName);
//        teacherForTest.setTotalSalary(teacherUpdatedTotalSalary);
//
//        when(teacherService.updateTeacher(teacherForTest.getNrg(), teacherForTest)).thenReturn(teacherForTest);
//
//        mvc.perform(put("/timetable/updateTeacher/" + teacherForTest.getNrg()).contentType(MediaType.APPLICATION_JSON)
//                .content(mapToJson(teacherForTest)))
//                .andExpect(status().isOk()).andExpect(content()
//                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.name").value(teacherUpdatedName));
//    }

    @Test
    void testControllerGetListTeacher() throws Exception {
        Iterable<Teacher> listTeacher = Arrays.asList(teacherForTest);
        when(teacherService.getListTeacher()).thenReturn(listTeacher);

        mvc.perform(get("/timetable/allTeacher").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].nrg").value(teacherForTest.getNrg()));
    }

    @Test
    void testControllerDeleteTeacher() throws Exception {
        teacherService.createTeacher(teacherForTest);
        mvc.perform(delete("/timetable/deleteTeacher/" + teacherForTest.getNrg()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }
}