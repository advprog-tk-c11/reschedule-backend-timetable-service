package com.advprog.reschedule.backendtimetableservice.tablePage.service;

import com.advprog.reschedule.backendtimetableservice.model.Classes;
import com.advprog.reschedule.backendtimetableservice.model.Session;
import com.advprog.reschedule.backendtimetableservice.model.Teacher;
import com.advprog.reschedule.backendtimetableservice.repository.ClassesRepository;
import com.advprog.reschedule.backendtimetableservice.repository.SessionRepository;
import com.advprog.reschedule.backendtimetableservice.repository.TeacherRepository;
import com.advprog.reschedule.backendtimetableservice.tablePage.core.SessionsHour;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ClassesServiceImplTest {
    @Mock
    private ClassesRepository classesRepository;

    @Mock
    private SessionRepository sessionRepository;

    @Mock
    private TeacherRepository teacherRepository;

    @InjectMocks
    private ClassesServiceImpl classesService;

    @InjectMocks
    private SessionServiceImpl sessionService;

    @InjectMocks
    private TeacherServiceImpl teacherService;

    private Classes classForTest;

    private Session sessionForTest;

    private Teacher teacherForTest;

    @BeforeEach
    public void setUp(){
        classForTest = new Classes();
        classForTest.setName("Dummy Class");
        classForTest.setCurrentDay("Senin");

        teacherForTest = new Teacher();
        teacherForTest.setNrg("1");
        teacherForTest.setName("Dummy Teacher");
        teacherForTest.setTotalSalary(0);

        sessionForTest = new Session();
        sessionForTest.setCourse("Dummy Course");
        sessionForTest.setDay("Senin");
        sessionForTest.setStartTime(LocalTime.parse("08:00:00"));
        sessionForTest.setEndTime(LocalTime.parse("09:00:00"));
        sessionForTest.setOnClass(classForTest);
        sessionForTest.setTeacher(teacherForTest);
        List<Session> teacherWorkOnSessionList = new ArrayList<Session>();
        teacherWorkOnSessionList.add(sessionForTest);
        teacherForTest.setWorkOnSessions(teacherWorkOnSessionList);
        teacherRepository.save(teacherForTest);
        List<Session> classAvailableSessionList = new ArrayList<Session>();
        classAvailableSessionList.add(sessionForTest);
        classForTest.setAvailableSessions(classAvailableSessionList);
        classesRepository.save(classForTest);
        sessionRepository.save(sessionForTest);

        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testServiceCreateClasses(){
        lenient().when(classesService.createClass(classForTest)).thenReturn(classForTest);
        Classes resultClass = classesService.createClass(classForTest);
        Assertions.assertEquals(classForTest.getName(), resultClass.getName());
    }

    @Test
    void testServiceGetClass(){
        lenient().when(classesService.getClass("Dummy Class")).thenReturn(classForTest);
        Classes fetchedClass = classesService.getClass("Dummy Class");
        Assertions.assertEquals(classForTest.getName(), fetchedClass.getName());
    }

    @Test
    void testServiceGetListClasses(){
        Iterable<Classes> listClasses = classesRepository.findAll();
        lenient().when(classesService.getListClasses()).thenReturn(listClasses);
        Iterable<Classes> listClassesResult = classesService.getListClasses();
        Assertions.assertIterableEquals(listClasses, listClassesResult);
    }

    @Test
    void testServiceUpdateClass(){
        classesService.createClass(classForTest);
        String classCurrentDayBeforeUpdate  = classForTest.getCurrentDay();
        Classes expectedClass = new Classes();
        String newClassCurrentDay = "Selasa";
        classForTest.setCurrentDay(newClassCurrentDay);
        expectedClass.setCurrentDay(newClassCurrentDay);
        expectedClass.setAvailableSessions(classForTest.getAvailableSessions());
        lenient().when(classesService.updateClass(classForTest.getName(), expectedClass)).thenReturn(classForTest);
        Classes resultClass = classesService.updateClass(classForTest.getName(), expectedClass);
        Assertions.assertNotEquals(resultClass.getCurrentDay(), classCurrentDayBeforeUpdate);
        Assertions.assertEquals(classForTest.getCurrentDay(), resultClass.getCurrentDay());
    }

    @Test
    public void testServiceDeleteClass(){
        classesService.createClass(classForTest);
        classesService.deleteClass(classForTest.getName());
        lenient().when(classesService.getClass(classForTest.getName())).thenReturn(null);
        Assertions.assertEquals(null, classesRepository.findByName(classForTest.getName()));
    }

//    @Test
//    public void testGetSessionsByName() throws Exception {
//        String classForTestName = classForTest.getName();
//        lenient().when(classesService.getSessionsByName(classForTestName));
//        List<SessionsHour> fetchedSessionsHour = classesService.getSessionsByName(classForTestName);
//        Session settedSession = fetchedSessionsHour.get(0).getMondaySession();
//        Assertions.assertEquals(sessionForTest, settedSession);
//    }

    @Test
    public void testServiceGenerateSessionsHour(){
        List<SessionsHour> fetchedSessionsHour = classesService.generateSessionsHour();
        Assertions.assertEquals("8:00", fetchedSessionsHour.get(0).getStartTime());
        Assertions.assertEquals("9:00", fetchedSessionsHour.get(1).getStartTime());
        Assertions.assertEquals("10:00", fetchedSessionsHour.get(2).getStartTime());
        Assertions.assertEquals("11:00", fetchedSessionsHour.get(3).getStartTime());
        Assertions.assertEquals("13:00", fetchedSessionsHour.get(4).getStartTime());
        Assertions.assertEquals("14:00", fetchedSessionsHour.get(5).getStartTime());
    }

    @Test
    public void testSetSessionsIntoSessionsHour(){
        List<SessionsHour> generatedSessionsHour = classesService.generateSessionsHour();
        Session sessionForTest = new Session();
        sessionForTest.setCourse("Dummy Course");
        sessionForTest.setDay("Senin");
        sessionForTest.setStartTime(LocalTime.parse("08:00:00"));
        sessionForTest.setEndTime(LocalTime.parse("09:00:00"));
        List<Session> sessionsListForTest = new ArrayList<Session>();
        sessionsListForTest.add(sessionForTest);

        lenient().when(classesService.setSessionsIntoSessionsHour(sessionsListForTest, generatedSessionsHour)).thenReturn(generatedSessionsHour);
        List<SessionsHour> fetchedSessionsHour = classesService.setSessionsIntoSessionsHour(sessionsListForTest, generatedSessionsHour);
        Session settedSession = fetchedSessionsHour.get(0).getMondaySession();
        Assertions.assertEquals(sessionForTest, settedSession);
    }

    @Test
    public void testIndexValueOfStartTime(){
        String startTimeForTest = "08:00";
        int fetchedIndexValueTestOne = classesService.indexValueOfStartTime(startTimeForTest);
        Assertions.assertEquals(0, fetchedIndexValueTestOne);

        String startTimeForTestTwo = "09:00";
        int fetchedIndexValueTestTwo = classesService.indexValueOfStartTime(startTimeForTestTwo);
        Assertions.assertEquals(1, fetchedIndexValueTestTwo);

        String startTimeForTestThree = "10:00";
        int fetchedIndexValueTestThree = classesService.indexValueOfStartTime(startTimeForTestThree);
        Assertions.assertEquals(2, fetchedIndexValueTestThree);

        String startTimeForTestFour = "11:00";
        int fetchedIndexValueTestFour = classesService.indexValueOfStartTime(startTimeForTestFour);
        Assertions.assertEquals(3, fetchedIndexValueTestFour);

        String startTimeForTestFive = "13:00";
        int fetchedIndexValueTestFive = classesService.indexValueOfStartTime(startTimeForTestFive);
        Assertions.assertEquals(4, fetchedIndexValueTestFive);

        String startTimeForTestSix = "14:00";
        int fetchedIndexValueTestSix = classesService.indexValueOfStartTime(startTimeForTestSix);
        Assertions.assertEquals(5, fetchedIndexValueTestSix);

        String startTimeForTestSeven = "15:00";
        int fetchedIndexValueTestSeven = classesService.indexValueOfStartTime(startTimeForTestSeven);
        Assertions.assertEquals(0, fetchedIndexValueTestSeven);
    }

    @Test
    public void testSetDaySession(){
        SessionsHour sessionsHourForTest = new SessionsHour("8:00", "9:00");

        Session sessionForTestMonday = new Session();
        sessionForTestMonday.setCourse("Dummy Course");
        sessionForTestMonday.setDay("Senin");
        sessionForTestMonday.setStartTime(LocalTime.parse("08:00:00"));
        sessionForTestMonday.setEndTime(LocalTime.parse("09:00:00"));
        classesService.setDaySession(sessionsHourForTest, sessionForTestMonday);
        Assertions.assertEquals(sessionForTestMonday, sessionsHourForTest.getMondaySession());

        Session sessionForTestTuesday = new Session();
        sessionForTestTuesday.setCourse("Dummy Course");
        sessionForTestTuesday.setDay("Selasa");
        sessionForTestTuesday.setStartTime(LocalTime.parse("08:00:00"));
        sessionForTestTuesday.setEndTime(LocalTime.parse("09:00:00"));
        classesService.setDaySession(sessionsHourForTest, sessionForTestTuesday);
        Assertions.assertEquals(sessionForTestTuesday, sessionsHourForTest.getTuesdaySession());

        Session sessionForTestWednesday = new Session();
        sessionForTestWednesday.setCourse("Dummy Course");
        sessionForTestWednesday.setDay("Rabu");
        sessionForTestWednesday.setStartTime(LocalTime.parse("08:00:00"));
        sessionForTestWednesday.setEndTime(LocalTime.parse("09:00:00"));
        classesService.setDaySession(sessionsHourForTest, sessionForTestWednesday);
        Assertions.assertEquals(sessionForTestWednesday, sessionsHourForTest.getWednesdaySession());

        Session sessionForTestThursday = new Session();
        sessionForTestThursday.setCourse("Dummy Course");
        sessionForTestThursday.setDay("Kamis");
        sessionForTestThursday.setStartTime(LocalTime.parse("08:00:00"));
        sessionForTestThursday.setEndTime(LocalTime.parse("09:00:00"));
        classesService.setDaySession(sessionsHourForTest, sessionForTestThursday);
        Assertions.assertEquals(sessionForTestThursday, sessionsHourForTest.getThursdaySession());

        Session sessionForTestFriday = new Session();
        sessionForTestFriday.setCourse("Dummy Course");
        sessionForTestFriday.setDay("Jumat");
        sessionForTestFriday.setStartTime(LocalTime.parse("08:00:00"));
        sessionForTestFriday.setEndTime(LocalTime.parse("09:00:00"));
        classesService.setDaySession(sessionsHourForTest, sessionForTestFriday);
        Assertions.assertEquals(sessionForTestFriday, sessionsHourForTest.getFridaySession());
    }
}

