package com.advprog.reschedule.backendtimetableservice.tablePage.service;

import com.advprog.reschedule.backendtimetableservice.model.Classes;
import com.advprog.reschedule.backendtimetableservice.model.Session;
import com.advprog.reschedule.backendtimetableservice.model.Teacher;
import com.advprog.reschedule.backendtimetableservice.repository.ClassesRepository;
import com.advprog.reschedule.backendtimetableservice.repository.SessionRepository;
import com.advprog.reschedule.backendtimetableservice.repository.TeacherRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class SessionServiceImplTest {
    @Mock
    private SessionRepository sessionRepository;

    @Mock
    private ClassesRepository classesRepository;

    @Mock
    private TeacherRepository teacherRepository;

    @InjectMocks
    private ClassesServiceImpl classesService;

    @InjectMocks
    private TeacherServiceImpl teacherService;

    @InjectMocks
    private SessionServiceImpl sessionService;

    private Classes classForTest;

    private Teacher teacherForTest;

    private Session sessionForTest;

    @BeforeEach
    public void setUp() {
        classForTest = new Classes();
        classForTest.setName("Dummy Class");
        classForTest.setCurrentDay("Senin");

        teacherForTest = new Teacher();
        teacherForTest.setNrg("1");
        teacherForTest.setName("Dummy Teacher");
        teacherForTest.setTotalSalary(0);
        sessionForTest = new Session();
        sessionForTest.setId(0);
        sessionForTest.setCourse("Dummy Course");
        sessionForTest.setDay("Senin");
        sessionForTest.setStartTime(LocalTime.parse("08:00:00"));
        sessionForTest.setEndTime(LocalTime.parse("09:00:00"));
        sessionForTest.setTeacher(teacherForTest);
        sessionForTest.setOnClass(classForTest);
        List<Session> teacherWorkOnSessionList = new ArrayList<Session>();
        teacherWorkOnSessionList.add(sessionForTest);
        teacherForTest.setWorkOnSessions(teacherWorkOnSessionList);
        teacherRepository.save(teacherForTest);
        List<Session> classAvailableSessionList = new ArrayList<Session>();
        classAvailableSessionList.add(sessionForTest);
        classForTest.setAvailableSessions(classAvailableSessionList);
        classesRepository.save(classForTest);
        sessionRepository.save(sessionForTest);

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testServiceCreateSession() throws Exception {
        when(classesService.getClass(classForTest.getName())).thenReturn(classForTest);
        when(teacherService.getTeacher(teacherForTest.getNrg())).thenReturn(teacherForTest);
        Session sessionForTestTwo = new Session();
        sessionForTestTwo.setId(1);
        sessionForTestTwo.setCourse("Dummy Course 2");
        sessionForTestTwo.setDay("Senin");
        sessionForTestTwo.setStartTime(LocalTime.parse("09:00:00"));
        sessionForTestTwo.setEndTime(LocalTime.parse("10:00:00"));
        sessionForTestTwo.setTeacher(teacherForTest);
        sessionForTestTwo.setOnClass(classForTest);
        lenient().when(sessionService.createSession(sessionForTestTwo, teacherForTest.getNrg(), classForTest.getName())).thenReturn(sessionForTestTwo);
    }

    @Test
    public void testServiceGetSession() {
        lenient().when(sessionService.getSessionById(0)).thenReturn(sessionForTest);
        Session fetchedSession = sessionService.getSessionById(0);
        Assertions.assertEquals(sessionForTest.getId(), fetchedSession.getId());
    }

    @Test
    void testServiceGetListSession() {
        Iterable<Session> listSession = sessionRepository.findAll();
        lenient().when(sessionService.getListSession()).thenReturn(listSession);
        Iterable<Session> listSessionResult = sessionService.getListSession();
        Assertions.assertIterableEquals(listSession, listSessionResult);
    }

    @Test
    public void testServiceUpdateSession() throws Exception {
        String sessionCourseBeforeUpdate = sessionForTest.getCourse();
        String newSessionCourse = "Dummy Course 2";
        String newSessionDay = "Selasa";
        LocalTime newSessionStartTime = LocalTime.parse("10:00:00");
        LocalTime newSessionEndTime = LocalTime.parse("11:00:00");
        Teacher newSessionTeacher = sessionForTest.getTeacher();
        Classes newSessionClass = sessionForTest.getOnClass();
        Session expectedSession = new Session();

        expectedSession.setCourse(newSessionCourse);
        expectedSession.setDay(newSessionDay);
        expectedSession.setStartTime(newSessionStartTime);
        expectedSession.setEndTime(newSessionEndTime);
        expectedSession.setTeacher(newSessionTeacher);
        expectedSession.setOnClass(newSessionClass);

        lenient().when(sessionService.updateSession(0, expectedSession)).thenReturn(expectedSession);
        Session resultSession = sessionService.updateSession(0, expectedSession);

        Assertions.assertNotEquals(resultSession.getCourse(), sessionCourseBeforeUpdate);
        Assertions.assertEquals(expectedSession.getCourse(), resultSession.getCourse());
    }

    @Test
    public void testDeleteSession() throws Exception {
        sessionService.deleteSessionById(0);
        lenient().when(sessionService.getSessionById(0)).thenReturn(null);
        Assertions.assertEquals(null, sessionService.getSessionById(0));
    }
}