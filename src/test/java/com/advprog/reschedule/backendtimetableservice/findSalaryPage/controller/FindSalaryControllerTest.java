package com.advprog.reschedule.backendtimetableservice.findSalaryPage.controller;

import com.advprog.reschedule.backendtimetableservice.findSalaryPage.core.GetDailySimulatedSalary;
import com.advprog.reschedule.backendtimetableservice.findSalaryPage.core.GetMonthlySimulatedSalary;
import com.advprog.reschedule.backendtimetableservice.findSalaryPage.core.GetWeeklySimulatedSalary;
import com.advprog.reschedule.backendtimetableservice.findSalaryPage.service.FindSalaryServiceImpl;
import com.advprog.reschedule.backendtimetableservice.model.Teacher;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = FindSalaryController.class)
public class FindSalaryControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private FindSalaryServiceImpl findSalaryService;

    @MockBean
    private GetDailySimulatedSalary getDailySimulatedSalary;

    @MockBean
    private GetWeeklySimulatedSalary getWeeklySimulatedSalary;

    @MockBean
    private GetMonthlySimulatedSalary getMonthlySimulatedSalary;

    private Teacher teacher;

    @BeforeEach
    public void setUp(){
        teacher = new Teacher();
        teacher.setNrg("1");
        teacher.setName("Teacher A");
    }

    @Test
    public void testControllerGetSalary() throws Exception {
        mvc.perform(get("/findSalary?nrg=1"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.teacherNrg").value("1"));
    }

    @Test
    public void testControllerAddSalaryDaily() throws Exception {
        mvc.perform(get("/findSalary/add-daily?nrg=1"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.teacherNrg").value("1"));
    }

    @Test
    public void testControllerAddSalaryWeekly() throws Exception {
            mvc.perform(get("/findSalary/add-weekly?nrg=1"))
                    .andExpect(status().isOk())
                    .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$.teacherNrg").value("1"));
    }

    @Test
    public void testControllerAddSalaryMonthly() throws Exception {
        mvc.perform(get("/findSalary/add-monthly?nrg=1"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.teacherNrg").value("1"));
    }
}
