package com.advprog.reschedule.backendtimetableservice.findSalaryPage.core;

import com.advprog.reschedule.backendtimetableservice.findSalaryPage.service.FindSalaryServiceImpl;
import com.advprog.reschedule.backendtimetableservice.model.Session;
import com.advprog.reschedule.backendtimetableservice.model.Teacher;
import com.advprog.reschedule.backendtimetableservice.repository.SessionRepository;
import com.advprog.reschedule.backendtimetableservice.repository.TeacherRepository;
import com.advprog.reschedule.backendtimetableservice.tablePage.service.TeacherServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class GetWeeklySimulatedSalaryTest {
    private Class<?> getWeeklySimulatedSalaryClass;

    @Mock
    private TeacherRepository teacherRepository;

    @Mock
    private SessionRepository sessionRepository;

    @Mock
    private GetDailySimulatedSalary getDailySimulatedSalary;

    @Mock
    private GetMonthlySimulatedSalary getMonthlySimulatedSalary;

    @InjectMocks
    private GetWeeklySimulatedSalary getWeeklySimulatedSalary;

    @Mock
    private TeacherServiceImpl teacherService;

    @InjectMocks
    private FindSalaryServiceImpl findSalaryService;

    private Teacher teacher;
    private Session session;
    private List<Session> sessions;

    @BeforeEach
    public void setUp() throws Exception {
        getWeeklySimulatedSalaryClass = Class.forName("com.advprog.reschedule.backendtimetableservice.findSalaryPage.core.GetWeeklySimulatedSalary");
        teacher = new Teacher("1", "Dummy");

        session = new Session();
        session.setCourse("Adprog");
        session.setDay(LocalDate.now().getDayOfWeek().getDisplayName(TextStyle.FULL, new Locale("id", "ID")));
        session.setTeacher(teacher);
        session.setStartTime(LocalTime.parse("08:00:00"));
        session.setEndTime(LocalTime.parse("09:00:00"));

        sessions = new ArrayList<>();
        sessions.add(session);
    }

    @Test
    public void testGetWeeklySimulatedSalaryTestIsAConcreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(getWeeklySimulatedSalaryClass.getModifiers()));
    }

    @Test
    public void testGetWeeklySimulatedSalaryTestIsAGetSimulatedSalary() throws Exception {
        Class<?> parentClass = getWeeklySimulatedSalaryClass.getSuperclass();

        assertEquals("com.advprog.reschedule.backendtimetableservice.findSalaryPage.core.GetSimulatedSalary", parentClass.getName());
    }

    @Test
    public void testOverrideUpdateSimulatedDate() throws Exception {
        Method updateSimulatedDate = getWeeklySimulatedSalaryClass.getDeclaredMethod("updateSimulatedDate");

        assertEquals(0, updateSimulatedDate.getParameterCount());
    }

    @Test
    public void testOverrideCalculateSalaryAddition() throws Exception {
        Method calculateSalaryAddition = getWeeklySimulatedSalaryClass.getDeclaredMethod("calculateSalaryAddition", Teacher.class);

        assertEquals("long", calculateSalaryAddition.getGenericReturnType().getTypeName());
        assertEquals(1, calculateSalaryAddition.getParameterCount());
    }

    @Test
    public void testAssignObservers() {
        getWeeklySimulatedSalary.assignObservers();
        assertEquals(2, getWeeklySimulatedSalary.observers.size());
        assertTrue(getWeeklySimulatedSalary.observers.contains(getDailySimulatedSalary));
        assertTrue(getWeeklySimulatedSalary.observers.contains(getMonthlySimulatedSalary));
    }

    @Test
    public void testNotifyTeacherChange() {
        getWeeklySimulatedSalary.assignObservers();
        getWeeklySimulatedSalary.setSimulatedTeacher(teacher);
        getWeeklySimulatedSalary.setSimulatedSalary(100);

        doCallRealMethod().when(getDailySimulatedSalary).setSimulatedTeacher(teacher);
        doCallRealMethod().when(getMonthlySimulatedSalary).setSimulatedTeacher(teacher);
        doCallRealMethod().when(getDailySimulatedSalary).setSimulatedSalary(100);
        doCallRealMethod().when(getMonthlySimulatedSalary).setSimulatedSalary(100);
        getWeeklySimulatedSalary.notifyTeacherChange();

        assertEquals(teacher, getDailySimulatedSalary.simulatedTeacher);
        assertEquals(teacher, getMonthlySimulatedSalary.simulatedTeacher);
        assertEquals(100, getDailySimulatedSalary.simulatedSalary);
        assertEquals(100, getMonthlySimulatedSalary.simulatedSalary);
    }

    @Test
    public void testUpdateSimulatedDate() {
        LocalDateTime simulationDate = LocalDateTime.now();
        getWeeklySimulatedSalary.setSimulatedDate(simulationDate);
        LocalDateTime sevenDaysAddition = simulationDate.plusDays(7);
        getWeeklySimulatedSalary.updateSimulatedDate();

        assertEquals(sevenDaysAddition, getWeeklySimulatedSalary.getSimulatedDate());
    }

    @Test
    public void testNotifySimulationResourcesChange() {
        getWeeklySimulatedSalary.assignObservers();
        LocalDateTime simulatedDate = LocalDateTime.now();
        getWeeklySimulatedSalary.setSimulatedDate(simulatedDate);
        getWeeklySimulatedSalary.setSimulatedSalary(100);

        doCallRealMethod().when(getDailySimulatedSalary).setSimulatedDate(simulatedDate);
        doCallRealMethod().when(getMonthlySimulatedSalary).setSimulatedDate(simulatedDate);
        doCallRealMethod().when(getDailySimulatedSalary).setSimulatedSalary(100);
        doCallRealMethod().when(getMonthlySimulatedSalary).setSimulatedSalary(100);
        getWeeklySimulatedSalary.notifySimulationResourcesChange();

        assertEquals(simulatedDate, getDailySimulatedSalary.simulatedDate);
        assertEquals(simulatedDate, getMonthlySimulatedSalary.simulatedDate);
        assertEquals(100, getDailySimulatedSalary.simulatedSalary);
        assertEquals(100, getMonthlySimulatedSalary.simulatedSalary);
    }

    @Test
    public void testCalculateSalaryAddition() throws Exception {
        lenient().when(sessionRepository.findByTeacher(teacher)).thenReturn(sessions);
        long addition = getWeeklySimulatedSalary.calculateSalaryAddition(teacher);

        assertEquals(100000, addition);
    }

    @Test
    public void testGetSalary() throws Exception {
        lenient().when(sessionRepository.findByTeacher(teacher)).thenReturn(sessions);
        long simulatedSalary = getWeeklySimulatedSalary.getSalary(teacher);

        assertEquals(100000, simulatedSalary);
    }
}
