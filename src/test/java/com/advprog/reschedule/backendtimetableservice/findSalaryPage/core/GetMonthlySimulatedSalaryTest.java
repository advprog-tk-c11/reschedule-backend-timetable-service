package com.advprog.reschedule.backendtimetableservice.findSalaryPage.core;

import com.advprog.reschedule.backendtimetableservice.findSalaryPage.service.FindSalaryServiceImpl;
import com.advprog.reschedule.backendtimetableservice.model.Session;
import com.advprog.reschedule.backendtimetableservice.model.Teacher;
import com.advprog.reschedule.backendtimetableservice.repository.SessionRepository;
import com.advprog.reschedule.backendtimetableservice.repository.TeacherRepository;
import com.advprog.reschedule.backendtimetableservice.tablePage.service.TeacherServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class GetMonthlySimulatedSalaryTest {
    private Class<?> getMonthlySimulatedSalaryClass;

    @Mock
    private TeacherRepository teacherRepository;

    @Mock
    private SessionRepository sessionRepository;

    @Mock
    private GetDailySimulatedSalary getDailySimulatedSalary;

    @Mock
    private GetWeeklySimulatedSalary getWeeklySimulatedSalary;

    @Mock
    private TeacherServiceImpl teacherService;

    @InjectMocks
    private GetMonthlySimulatedSalary getMonthlySimulatedSalary;

    @InjectMocks
    private FindSalaryServiceImpl findSalaryService;

    private Teacher teacher;
    private Session session;
    private List<Session> sessions;

    @BeforeEach
    public void setUp() throws Exception {
        getMonthlySimulatedSalaryClass = Class.forName("com.advprog.reschedule.backendtimetableservice.findSalaryPage.core.GetMonthlySimulatedSalary");
        teacher = new Teacher("1", "Dummy");

        session = new Session();
        session.setCourse("Adprog");
        session.setDay(LocalDate.now().getDayOfWeek().getDisplayName(TextStyle.FULL, new Locale("id", "ID")));
        session.setTeacher(teacher);
        session.setStartTime(LocalTime.parse("08:00:00"));
        session.setEndTime(LocalTime.parse("09:00:00"));

        sessions = new ArrayList<>();
        sessions.add(session);
    }

    @Test
    public void testGetMonthlySimulatedSalaryTestIsAConcreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(getMonthlySimulatedSalaryClass.getModifiers()));
    }

    @Test
    public void testGetMonthlySimulatedSalaryTestIsAGetSimulatedSalary() throws Exception {
        Class<?> parentClass = getMonthlySimulatedSalaryClass.getSuperclass();

        assertEquals("com.advprog.reschedule.backendtimetableservice.findSalaryPage.core.GetSimulatedSalary", parentClass.getName());
    }

    @Test
    public void testOverrideUpdateSimulatedDate() throws Exception {
        Method updateSimulatedDate = getMonthlySimulatedSalaryClass.getDeclaredMethod("updateSimulatedDate");

        assertEquals(0, updateSimulatedDate.getParameterCount());
    }

    @Test
    public void testOverrideCalculateSalaryAddition() throws Exception {
        Method calculateSalaryAddition = getMonthlySimulatedSalaryClass.getDeclaredMethod("calculateSalaryAddition", Teacher.class);

        assertEquals("long", calculateSalaryAddition.getGenericReturnType().getTypeName());
        assertEquals(1, calculateSalaryAddition.getParameterCount());
    }

    @Test
    public void testAssignObservers() {
        getMonthlySimulatedSalary.assignObservers();
        assertEquals(2, getMonthlySimulatedSalary.observers.size());
        assertTrue(getMonthlySimulatedSalary.observers.contains(getDailySimulatedSalary));
        assertTrue(getMonthlySimulatedSalary.observers.contains(getWeeklySimulatedSalary));
    }

    @Test
    public void testNotifyTeacherChange() {
        getMonthlySimulatedSalary.assignObservers();
        getMonthlySimulatedSalary.setSimulatedTeacher(teacher);
        getMonthlySimulatedSalary.setSimulatedSalary(100);

        doCallRealMethod().when(getDailySimulatedSalary).setSimulatedTeacher(teacher);
        doCallRealMethod().when(getWeeklySimulatedSalary).setSimulatedTeacher(teacher);
        doCallRealMethod().when(getDailySimulatedSalary).setSimulatedSalary(100);
        doCallRealMethod().when(getWeeklySimulatedSalary).setSimulatedSalary(100);
        getMonthlySimulatedSalary.notifyTeacherChange();

        assertEquals(teacher, getDailySimulatedSalary.simulatedTeacher);
        assertEquals(teacher, getWeeklySimulatedSalary.simulatedTeacher);
        assertEquals(100, getDailySimulatedSalary.simulatedSalary);
        assertEquals(100, getWeeklySimulatedSalary.simulatedSalary);
    }

    @Test
    public void testUpdateSimulatedDateTwentyEightDaysMonth() {
        LocalDateTime simulationDate = LocalDateTime.of(2019, 2, 1, 8, 0);
        getMonthlySimulatedSalary.setSimulatedDate(simulationDate);
        LocalDateTime twentyEightDaysAddition = simulationDate.plusDays(28);
        getMonthlySimulatedSalary.updateSimulatedDate();

        assertEquals(twentyEightDaysAddition, getMonthlySimulatedSalary.getSimulatedDate());
    }

    @Test
    public void testUpdateSimulatedDateTwentyNineDaysMonth() {
        LocalDateTime simulationDate = LocalDateTime.of(2020, 2, 1, 8, 0);
        getMonthlySimulatedSalary.setSimulatedDate(simulationDate);
        LocalDateTime twentyNineDaysAddition = simulationDate.plusDays(29);
        getMonthlySimulatedSalary.updateSimulatedDate();

        assertEquals(twentyNineDaysAddition, getMonthlySimulatedSalary.getSimulatedDate());
    }

    @Test
    public void testUpdateSimulatedDateThirtyDaysMonth() {
        LocalDateTime simulationDate = LocalDateTime.of(2020, 4, 1, 8, 0);
        getMonthlySimulatedSalary.setSimulatedDate(simulationDate);
        LocalDateTime thirtyDaysAddition = simulationDate.plusDays(30);
        getMonthlySimulatedSalary.updateSimulatedDate();

        assertEquals(thirtyDaysAddition, getMonthlySimulatedSalary.getSimulatedDate());
    }

    @Test
    public void testUpdateSimulatedDateThirtyOneDaysMonth() {
        LocalDateTime simulationDate = LocalDateTime.of(2020, 5, 1, 8, 0);
        getMonthlySimulatedSalary.setSimulatedDate(simulationDate);
        LocalDateTime thirtyOneDaysAddition = simulationDate.plusDays(31);
        getMonthlySimulatedSalary.updateSimulatedDate();

        assertEquals(thirtyOneDaysAddition, getMonthlySimulatedSalary.getSimulatedDate());
    }

    @Test
    public void testGetTargetDateFromEndOfJuly() {
        LocalDateTime simulationDate = LocalDateTime.of(2020, 7, 31, 8, 0);
        getMonthlySimulatedSalary.setSimulatedDate(simulationDate);
        LocalDateTime augustThirtyFirst = LocalDateTime.of(2020, 8, 31, 8, 0);
        getMonthlySimulatedSalary.updateSimulatedDate();

        assertEquals(augustThirtyFirst, getMonthlySimulatedSalary.getSimulatedDate());
    }

    @Test
    public void testGetTargetDateFromEndOfDecember() {
        LocalDateTime simulationDate = LocalDateTime.of(2020, 12, 31, 8, 0);
        getMonthlySimulatedSalary.setSimulatedDate(simulationDate);
        LocalDateTime januaryThirtyFirst = LocalDateTime.of(2021, 1, 31, 8, 0);
        getMonthlySimulatedSalary.updateSimulatedDate();

        assertEquals(januaryThirtyFirst, getMonthlySimulatedSalary.getSimulatedDate());
    }

    @Test
    public void testGetTargetDateFromEndOfOtherThirtyOneDayMonths() {
        LocalDateTime simulationDate = LocalDateTime.of(2020, 8, 31, 8, 0);
        getMonthlySimulatedSalary.setSimulatedDate(simulationDate);
        LocalDateTime septemberThirtieth = LocalDateTime.of(2020, 9, 30, 8, 0);
        getMonthlySimulatedSalary.updateSimulatedDate();

        assertEquals(septemberThirtieth, getMonthlySimulatedSalary.getSimulatedDate());
    }

    @Test
    public void testGetTargetDateFromEndOfJanuaryNonLeapYear() {
        LocalDateTime simulationDate = LocalDateTime.of(2021, 1, 31, 8, 0);
        getMonthlySimulatedSalary.setSimulatedDate(simulationDate);
        LocalDateTime februaryTwentyEighth = LocalDateTime.of(2021, 2, 28, 8, 0);
        getMonthlySimulatedSalary.updateSimulatedDate();

        assertEquals(februaryTwentyEighth, getMonthlySimulatedSalary.getSimulatedDate());
    }

    @Test
    public void testGetTargetDateFromEndOfJanuaryLeapYear() {
        LocalDateTime simulationDate = LocalDateTime.of(2020, 1, 31, 8, 0);
        getMonthlySimulatedSalary.setSimulatedDate(simulationDate);
        LocalDateTime februaryTwentyNinth = LocalDateTime.of(2020, 2, 29, 8, 0);
        getMonthlySimulatedSalary.updateSimulatedDate();

        assertEquals(februaryTwentyNinth, getMonthlySimulatedSalary.getSimulatedDate());
    }

    @Test
    public void testNotifySimulationResourcesChange() {
        getMonthlySimulatedSalary.assignObservers();
        LocalDateTime simulatedDate = LocalDateTime.now();
        getMonthlySimulatedSalary.setSimulatedDate(simulatedDate);
        getMonthlySimulatedSalary.setSimulatedSalary(100);

        doCallRealMethod().when(getDailySimulatedSalary).setSimulatedDate(simulatedDate);
        doCallRealMethod().when(getWeeklySimulatedSalary).setSimulatedDate(simulatedDate);
        doCallRealMethod().when(getDailySimulatedSalary).setSimulatedSalary(100);
        doCallRealMethod().when(getWeeklySimulatedSalary).setSimulatedSalary(100);
        getMonthlySimulatedSalary.notifySimulationResourcesChange();

        assertEquals(simulatedDate, getDailySimulatedSalary.simulatedDate);
        assertEquals(simulatedDate, getWeeklySimulatedSalary.simulatedDate);
        assertEquals(100, getDailySimulatedSalary.simulatedSalary);
        assertEquals(100, getWeeklySimulatedSalary.simulatedSalary);
    }

    @Test
    public void testCalculateSalaryAddition() throws Exception {
        lenient().when(getWeeklySimulatedSalary.calculateSalaryAddition(teacher)).thenReturn(100000L);
        long addition = getMonthlySimulatedSalary.calculateSalaryAddition(teacher);

        assertEquals(400000, addition);
    }

    @Test
    public void testGetSalary() throws Exception {
        lenient().when(getWeeklySimulatedSalary.calculateSalaryAddition(teacher)).thenReturn(100000L);
        long simulatedSalary = getMonthlySimulatedSalary.getSalary(teacher);

        assertEquals(400000, simulatedSalary);
    }
}
