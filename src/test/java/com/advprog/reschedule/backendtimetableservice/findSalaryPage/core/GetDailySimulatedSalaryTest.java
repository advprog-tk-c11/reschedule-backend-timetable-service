package com.advprog.reschedule.backendtimetableservice.findSalaryPage.core;

import com.advprog.reschedule.backendtimetableservice.findSalaryPage.service.FindSalaryServiceImpl;
import com.advprog.reschedule.backendtimetableservice.model.Session;
import com.advprog.reschedule.backendtimetableservice.model.Teacher;
import com.advprog.reschedule.backendtimetableservice.repository.SessionRepository;
import com.advprog.reschedule.backendtimetableservice.repository.TeacherRepository;
import com.advprog.reschedule.backendtimetableservice.tablePage.service.TeacherServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class GetDailySimulatedSalaryTest {
    private Class<?> getDailySimulatedSalaryClass;

    @Mock
    private TeacherRepository teacherRepository;

    @Mock
    private SessionRepository sessionRepository;

    @InjectMocks
    private GetDailySimulatedSalary getDailySimulatedSalary;

    @Mock
    private GetWeeklySimulatedSalary getWeeklySimulatedSalary;

    @Mock
    private GetMonthlySimulatedSalary getMonthlySimulatedSalary;

    @Mock
    private TeacherServiceImpl teacherService;

    @InjectMocks
    private FindSalaryServiceImpl findSalaryService;

    private Teacher teacher;
    private Session session;
    private List<Session> sessions;

    @BeforeEach
    public void setUp() throws Exception {
        getDailySimulatedSalaryClass = Class.forName("com.advprog.reschedule.backendtimetableservice.findSalaryPage.core.GetDailySimulatedSalary");
        teacher = new Teacher("1", "Dummy");

        session = new Session();
        session.setCourse("Adprog");
        session.setDay(LocalDate.now().getDayOfWeek().getDisplayName(TextStyle.FULL, new Locale("id", "ID")));
        session.setTeacher(teacher);
        session.setStartTime(LocalTime.parse("08:00:00"));
        session.setEndTime(LocalTime.parse("09:00:00"));

        sessions = new ArrayList<>();
        sessions.add(session);
    }

    @Test
    public void testGetDailySimulatedSalaryTestIsAConcreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(getDailySimulatedSalaryClass.getModifiers()));
    }

    @Test
    public void testGetDailySimulatedSalaryTestIsAGetSimulatedSalary() throws Exception {
        Class<?> parentClass = getDailySimulatedSalaryClass.getSuperclass();

        assertEquals("com.advprog.reschedule.backendtimetableservice.findSalaryPage.core.GetSimulatedSalary", parentClass.getName());
    }

    @Test
    public void testOverrideUpdateSimulatedDate() throws Exception {
        Method updateSimulatedDate = getDailySimulatedSalaryClass.getDeclaredMethod("updateSimulatedDate");

        assertEquals(0, updateSimulatedDate.getParameterCount());
    }

    @Test
    public void testOverrideCalculateSalaryAddition() throws Exception {
        Method calculateSalaryAddition = getDailySimulatedSalaryClass.getDeclaredMethod("calculateSalaryAddition", Teacher.class);

        assertEquals("long", calculateSalaryAddition.getGenericReturnType().getTypeName());
        assertEquals(1, calculateSalaryAddition.getParameterCount());
    }

    @Test
    public void testAssignObservers() {
        getDailySimulatedSalary.assignObservers();
        assertEquals(2, getDailySimulatedSalary.observers.size());
        assertTrue(getDailySimulatedSalary.observers.contains(getWeeklySimulatedSalary));
        assertTrue(getDailySimulatedSalary.observers.contains(getMonthlySimulatedSalary));
    }

    @Test
    public void testNotifyTeacherChange() {
        getDailySimulatedSalary.assignObservers();
        getDailySimulatedSalary.setSimulatedTeacher(teacher);
        getDailySimulatedSalary.setSimulatedSalary(100);

        doCallRealMethod().when(getWeeklySimulatedSalary).setSimulatedTeacher(teacher);
        doCallRealMethod().when(getMonthlySimulatedSalary).setSimulatedTeacher(teacher);
        doCallRealMethod().when(getWeeklySimulatedSalary).setSimulatedSalary(100);
        doCallRealMethod().when(getMonthlySimulatedSalary).setSimulatedSalary(100);
        getDailySimulatedSalary.notifyTeacherChange();

        assertEquals(teacher, getWeeklySimulatedSalary.simulatedTeacher);
        assertEquals(teacher, getMonthlySimulatedSalary.simulatedTeacher);
        assertEquals(100, getWeeklySimulatedSalary.simulatedSalary);
        assertEquals(100, getMonthlySimulatedSalary.simulatedSalary);
    }

    @Test
    public void testUpdateSimulatedDate() {
        LocalDateTime simulationDate = LocalDateTime.now();
        getDailySimulatedSalary.setSimulatedDate(simulationDate);
        LocalDateTime oneDayAddition = simulationDate.plusDays(1);
        getDailySimulatedSalary.updateSimulatedDate();

        assertEquals(oneDayAddition, getDailySimulatedSalary.getSimulatedDate());
    }

    @Test
    public void testNotifySimulationResourcesChange() {
        getDailySimulatedSalary.assignObservers();
        LocalDateTime simulatedDate = LocalDateTime.now();
        getDailySimulatedSalary.setSimulatedDate(simulatedDate);
        getDailySimulatedSalary.setSimulatedSalary(100);

        doCallRealMethod().when(getWeeklySimulatedSalary).setSimulatedDate(simulatedDate);
        doCallRealMethod().when(getMonthlySimulatedSalary).setSimulatedDate(simulatedDate);
        doCallRealMethod().when(getWeeklySimulatedSalary).setSimulatedSalary(100);
        doCallRealMethod().when(getMonthlySimulatedSalary).setSimulatedSalary(100);
        getDailySimulatedSalary.notifySimulationResourcesChange();

        assertEquals(simulatedDate, getWeeklySimulatedSalary.simulatedDate);
        assertEquals(simulatedDate, getMonthlySimulatedSalary.simulatedDate);
        assertEquals(100, getWeeklySimulatedSalary.simulatedSalary);
        assertEquals(100, getMonthlySimulatedSalary.simulatedSalary);
    }

    @Test
    public void testCalculateSalaryAddition() throws Exception {
        lenient().when(sessionRepository.findByTeacherAndDay(teacher, session.getDay())).thenReturn(sessions);
        long addition = getDailySimulatedSalary.calculateSalaryAddition(teacher);

        assertEquals(100000, addition);
    }

    @Test
    public void testGetSalary() throws Exception {
        lenient().when(sessionRepository.findByTeacherAndDay(teacher, session.getDay())).thenReturn(sessions);
        long simulatedSalary = getDailySimulatedSalary.getSalary(teacher);

        assertEquals(100000, simulatedSalary);
    }
}
