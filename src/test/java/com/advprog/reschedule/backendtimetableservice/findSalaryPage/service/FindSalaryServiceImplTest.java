package com.advprog.reschedule.backendtimetableservice.findSalaryPage.service;

import com.advprog.reschedule.backendtimetableservice.findSalaryPage.core.GetDailySimulatedSalary;
import com.advprog.reschedule.backendtimetableservice.findSalaryPage.core.GetMonthlySimulatedSalary;
import com.advprog.reschedule.backendtimetableservice.findSalaryPage.core.GetWeeklySimulatedSalary;
import com.advprog.reschedule.backendtimetableservice.model.Session;
import com.advprog.reschedule.backendtimetableservice.model.Teacher;
import com.advprog.reschedule.backendtimetableservice.repository.SessionRepository;
import com.advprog.reschedule.backendtimetableservice.repository.TeacherRepository;
import com.advprog.reschedule.backendtimetableservice.tablePage.service.TeacherServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class FindSalaryServiceImplTest {
    @Mock
    private TeacherRepository teacherRepository;

    @Mock
    private SessionRepository sessionRepository;

    @Mock
    private TeacherServiceImpl teacherService;

    @Mock
    private GetDailySimulatedSalary getDailySimulatedSalary;

    @Mock
    private GetWeeklySimulatedSalary getWeeklySimulatedSalary;

    @Mock
    private GetMonthlySimulatedSalary getMonthlySimulatedSalary;

    @InjectMocks
    private FindSalaryServiceImpl findSalaryService;

    private Teacher teacher;
    private Session session;
    private List<Session> sessions;

    @BeforeEach
    public void setUp() {
        teacher = new Teacher();
        teacher.setNrg("1");
        teacher.setName("Teacher A");

        session = new Session();
        session.setCourse("Adprog");
        session.setDay(LocalDate.now().getDayOfWeek().getDisplayName(TextStyle.FULL, new Locale("id", "ID")));
        session.setTeacher(teacher);
        session.setStartTime(LocalTime.parse("08:00:00"));
        session.setEndTime(LocalTime.parse("09:00:00"));

        sessions = new ArrayList<>();
        sessions.add(session);
    }

    @Test
    public void testAssignObservers() {
        findSalaryService.assignObservers();
        assertEquals(3, findSalaryService.observers.size());
        assertTrue(findSalaryService.observers.contains(getDailySimulatedSalary));
        assertTrue(findSalaryService.observers.contains(getWeeklySimulatedSalary));
        assertTrue(findSalaryService.observers.contains(getMonthlySimulatedSalary));
    }

    @Test
    public void testGetSalary() {
        lenient().when(teacherService.getTeacher(teacher.getNrg())).thenReturn(teacher);
        long salary = findSalaryService.getSalary(teacher.getNrg());

        assertEquals(salary, 0);
    }

    @Test
    public void testGetDailySimulatedSalary() {
        lenient().when(teacherService.getTeacher(teacher.getNrg())).thenReturn(teacher);
        lenient().when(sessionRepository.findByTeacherAndDay(teacher, session.getDay())).thenReturn(sessions);
        long simulatedSalary = findSalaryService.getSimulatedSalary("1", "daily");

        assertEquals(0, simulatedSalary);
    }

    @Test
    public void testGetWeeklySimulatedSalary() {
        lenient().when(teacherService.getTeacher(teacher.getNrg())).thenReturn(teacher);
        long simulatedSalary = findSalaryService.getSimulatedSalary("1", "weekly");

        assertEquals(0, simulatedSalary);
    }

    @Test
    public void testGetMonthlySimulatedSalary() {
        lenient().when(teacherService.getTeacher(teacher.getNrg())).thenReturn(teacher);
        long simulatedSalary = findSalaryService.getSimulatedSalary("1", "monthly");

        assertEquals(0, simulatedSalary);
    }

    @Test
    public void testResetSimulation() {
        findSalaryService.assignObservers();
        doCallRealMethod().when(getDailySimulatedSalary).setSimulatedTeacher(null);
        doCallRealMethod().when(getWeeklySimulatedSalary).setSimulatedTeacher(null);
        doCallRealMethod().when(getMonthlySimulatedSalary).setSimulatedTeacher(null);
        findSalaryService.resetSimulation();

        doCallRealMethod().when(getDailySimulatedSalary).getSimulatedTeacher();
        doCallRealMethod().when(getWeeklySimulatedSalary).getSimulatedTeacher();
        doCallRealMethod().when(getMonthlySimulatedSalary).getSimulatedTeacher();

        assertEquals(null, getDailySimulatedSalary.getSimulatedTeacher());
        assertEquals(null, getWeeklySimulatedSalary.getSimulatedTeacher());
        assertEquals(null, getMonthlySimulatedSalary.getSimulatedTeacher());
    }

    @Test
    public void testGetSimulationDate() {
        lenient().when(getDailySimulatedSalary.getSimulatedDate()).thenReturn(LocalDateTime.now());
        LocalDate simulationDate = findSalaryService.getSimulationDate();

        assertEquals(LocalDate.now(), simulationDate);
    }
}
